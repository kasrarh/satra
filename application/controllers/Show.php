<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Show extends CI_Controller
{



	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portals/Admins/AdminsModel', "AdminsModel");
		$this->load->model('IndexModel', "IndexModel");
		$this->load->Library('Utility');
		$this->load->Library('AuthLib');
	}


	public function index()
	{
		$obj = new Utility();
		$category1 = $obj->getAll('category');
		$category2 = $this->AdminsModel->getCategory2();
		$items = $this->AdminsModel->getItems();
		$this->load->view('landing', array('category2' => $category2, 'items' => $items));
	}
	public function index2()
	{
		$obj = new Utility();
		$category1 = $obj->getAll('category');
		$category2 = $this->AdminsModel->getCategory2();
		$items = $this->AdminsModel->getItems();
		$this->load->view('index2', array('category2' => $category2, 'items' => $items));
	}
	public function vip()
	{
		$obj = new Utility();
		$category1 = $obj->getAll('category');
		$category2 = $this->AdminsModel->getCategory2();
		$items = $this->AdminsModel->getItems();
		$this->load->view('vip', array('category2' => $category2, 'items' => $items));
		}

	public function showShoppingCart(){
		$this->load->view('shopping-cart');

	}
	public function getItemsLocalstorage(){
		$data=json_decode($this->input->post('data'));
		// die(var_dump($data));
		$obj= new Utility();
		$info=[];
		foreach ($data as $d){
			$res=$obj->getWithId('items',$d->id,'id');
			$res[]=['count'=>$d->count];
			$info[]=$res;
		}
		echo json_encode($info);

	}
	public function submitCustomerFactor(){
		$data=json_decode($this->input->post('data'));
		$info=json_decode($this->input->post('info'));
		$obj= new Utility();
		$user= new AuthLib();
		$items=[];
		foreach ($data as $d){
			$res=$obj->getWithId('items',$d->id,'id');
			$res[]=['count'=>$d->count];
			$items[]=$res;
		}
		$address=$info->address;
		// die(var_dump($address));
			$newCustomer=array(
				'username'=>$info->phone,
				'name'=>$info->name,
				'password'=>$user->hash($info->phone),
				'address'=>$info->address,
				'phone'=>$info->phone,
			);
			$customers=$obj->getWithId('customers',$newCustomer['username'],'username');
			if($customers==null){
				$this->IndexModel->add('customers',$newCustomer);
				$id=$this->IndexModel->getLatestCustomer();
				$_SESSION['isCustomer']="true";
				$_SESSION['username']=$newCustomer['username'];
				$_SESSION['role']='customer';
			}else{
				$id=$customers[0]['customer_id'];
				$_SESSION['role']='customer';
				$_SESSION['isCustomer']="true";
				$_SESSION['username']=$newCustomer['username'];
			}
		
		$totalPrice=0;
			foreach ($items as $i) {
				$totalPrice=$totalPrice+($i[0]['price_normal']*$i[1]['count']);
			}
			$factor=array(
				'total_price'=>$totalPrice,
				'customer_id'=>$id,
				'address'=>$info->address
			);
			$this->IndexModel->add('factor',$factor);
			
			$latestFactorRow=$this->IndexModel->getLatestFactorId();			
			foreach ($items as $i) {
				$factorDetail=array(
					'factor_id'=>$latestFactorRow,
					'product_id'=>$i[0]['id'],
					'price'=>$i[0]['price_normal'],
					'amount'=>$i[1]['count']
				);
				$this->IndexModel->add('factor_detail',$factorDetail);
			}
			
			$_SESSION['TotalPrice'] = $totalPrice;
			$_SESSION['factor_id'] = $latestFactorRow;
			echo json_encode(true);


	}
	public function submitUserFactor(){
		
		$user= new AuthLib();
		$id=$user->GetIdBySession('users', 'user_id');
		$data=json_decode($this->input->post('data'));
		$info=json_decode($this->input->post('info'));
		$obj= new Utility();
		$items=[];
		foreach ($data as $d){
			$res=$obj->getWithId('items',$d->id,'id');
			$res[]=['count'=>$d->count];
			$items[]=$res;
		}
		$address=$info->address;
		$totalPrice=0;
			foreach ($items as $i) {
				$totalPrice=$totalPrice+($i[0]['price_special']*$i[1]['count']);
			}
			$factor=array(
				'total_price'=>$totalPrice,
				'user_id'=>$id,
				'address'=>$address
			);
			$this->IndexModel->add('factor',$factor);
			$latestFactorRow=$this->IndexModel->getLatestFactorId();
			foreach ($items as $i) {
				$factorDetail=array(
					'factor_id'=>$latestFactorRow,
					'product_id'=>$i[0]['id'],
					'price'=>$i[0]['price_special'],
					'amount'=>$i[1]['count']
				);
				$this->IndexModel->add('factor_detail',$factorDetail);
			}
			$_SESSION['TotalPrice'] = $totalPrice;
			$_SESSION['factor_id'] = $latestFactorRow;
			echo json_encode('true');
	}

}
