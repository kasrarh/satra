<?php
include_once 'application/helpers/jalaliconvertor.php';


class Show extends CI_Controller
{

	public function __construct()
	{
	parent::__construct();
    	$this->load->model('IndexModel');
    	$this->load->model("AuthModel");
	    $this->load->library('Utility');
		$this->load->database();
		$this->load->library('AuthLib');
		$User = new AuthLib();
		if ($_SESSION['role'] == 'admin'){

		}else{
			redirect(base_url('Auth/index'));
		}
//		$token = $User->getToken('admin');
//		$User->checkToken($token, $_SESSION['token'], 'Auth/LoginAdmin');
//		$User->isLogin('isAdmin', 'Auth/LoginAdmin');
//		$image = $User->GetPhotoBySession('admin', 'image_url');
	}

	public function addCategory()
	{
		$obj= new Utility();
		$this->load->view('dashboard/add-category');
		if (isset($_POST['submit'])){
			$img=$obj->uploadFile2('uploads/category/','files');
			$data=array(
				'name'=>$_POST['name'],
				'image_url'=>$img,
			);
			$res=$this->IndexModel->add('category',$data);
			if ($res){
				redirect(base_url('Dashboard/Show/showCategory'));
			}
		}
	}

	public function showCategory()
	{
		$obj=new Utility();
		$category = $obj->getAll('category');
		$this->load->view('dashboard/show-category',array('category'=>$category));

	}
	public function editCategory()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$category=$obj->getWithId('category',$id,'category_id');
		//die(var_dump($category));
		$this->load->view('dashboard/edit-category',array('category'=>$category));
		if (isset($_POST['submit'])){
			$img=$obj->uploadFile2('uploads/category/','files');
			$data=array(
				'name'=>$_POST['name'],
				'image_url'=>$img,
			);
			if ($img==null){
				unset($data['image_url']);
			}
			$res=$this->IndexModel->edit('category',$id,'category_id',$data);
			if ($res){
				redirect(base_url('Dashboard/Show/showCategory'));
			}
		}

	}
	public function deleteCategory()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$category=$obj->delete(base_url('Dashboard/Show/showCategory'),'category','category_id',$id);
	}



	public function addItem()
	{
		$obj = new Utility();
		$category=$obj->getAll('category');
		$this->load->view('dashboard/add-item',array('category'=>$category ));
		if (isset($_POST['submit'])){
			$img=$obj->uploadFile2('uploads/category/','files');
			$data=array(
				'name'=>$_POST['name'],
				'description'=>$_POST['description'],
				'price_normal'=>$_POST['price_normal'],
				'price_special'=>$_POST['price_special'],
				'category_id'=>$_POST['category_id'],
				'image_url'=>$img,


			);
			$res=$this->IndexModel->add('items',$data);
			if ($res){
				redirect(base_url('Dashboard/Show/showItem'));
			}
		}
	}

	public function showItem()
	{
		$obj= new Utility();
		$obj->getAllJoin('dashboard/show-item','items','category','category_id','category_id','category.name as c_name','category.image_url as c_img','items.name as i_name','items.image_url as i_img');

	}
	public function editItem()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$item=$obj->getWithId('items',$id,'id');
		$category=$obj->getAll('category');

		$this->load->view('dashboard/edit-item',array('category'=>$category,'item'=>$item));
		if (isset($_POST['submit'])){
			$img=$obj->uploadFile2('uploads/category/','files');
			$data=array(
				'name'=>$_POST['name'],
				'description'=>$_POST['description'],
				'price_normal'=>$_POST['price_normal'],
				'price_special'=>$_POST['price_special'],
				'category_id'=>$_POST['category_id'],
				'image_url'=>$img,
			);
			if ($img==null){
				unset($data['image_url']);
			}
			$res=$this->IndexModel->edit('items',$id,'id',$data);
			if ($res){
				redirect(base_url('Dashboard/Show/showItem'));
			}
		}

	}
	public function deleteItem()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$category=$obj->delete(base_url('Dashboard/Show/showItem'),'items','id',$id);
	}
	public function addUser()
	{
		$obj = new Utility();
		$user = new AuthLib();
		$info = array('email' => $this->input->post('email'), 'phone' => $this->input->post('phone'), 'address' => $this->input->post('address'), 'password' => $user->hash($this->input->post('password')), 'store_name' => $this->input->post('store_name'), 'fullname' => $this->input->post('fullname'), 'username' =>  $this->input->post('username'));
		$obj->add('dashboard/add-user', $info, 'users', base_url('Dashboard/Show/showUser'));

	}
	public function showUser()
	{
		$obj=new Utility();
		$user = $obj->getAll('users');
		$this->load->view('dashboard/show-user',array('user'=>$user));
	}
	public function deleteUser()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$category=$obj->delete(base_url('Dashboard/Show/showUser'),'users','user_id',$id);
	}

	public function editUser()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$user=$obj->getWithId('users',$id,'user_id');

		$this->load->view('dashboard/edit-user',array('user'=>$user));
		if (isset($_POST['submit'])){
			$data=array(
				'email'=>$_POST['email'],
				'phone'=>$_POST['phone'],
				'address'=>$_POST['address'],
				'store_name'=>$_POST['store_name'],
				'fullname'=>$_POST['fullname'],
				'username'=>$_POST['username'],
			);

			$res=$this->IndexModel->edit('users',$id,'user_id',$data);
			if ($res){
				redirect(base_url('Dashboard/Show/showUser'));
			}
		}

	}

	public function showCustomer()
	{
		$obj=new Utility();
		$customer = $obj->getAll('customers');
		$this->load->view('dashboard/show-customer',array('customer'=>$customer));
	}

	

	public function showCustomerFactor()
	{
		$obj=new Utility();
		$obj->getAllJoin2('dashboard/show-customer-factor','factor','customers','customer_id','customer_id','factor.address as f_address','factor.created_at as factor_date');
		
	}
	public function showTodayCustomerFactor(){
		$end = date("Y-m-d 23:59:59");
		$start = date("Y-m-d 00:00:00");
		$data = $this->IndexModel->getCustomerFactor($end,$start);
		$this->load->view('dashboard/show-customer-factor',array('data'=>$data));
	}
	public function showTodayUserFactor(){
		$end = date("Y-m-d 23:59:59");
		$start = date("Y-m-d 00:00:00");
		$data = $this->IndexModel->getUserFactor($end,$start);
		$this->load->view('dashboard/show-user-factor',array('data'=>$data));
	}
	public function showUserFactor()
	{
		$obj=new Utility();
		$obj->getAllJoin2('dashboard/show-user-factor','factor','users','user_id','user_id','factor.address as f_address','factor.created_at as factor_date');
	}
	public function showFactorDetail()
	{
		$id= $this->uri->segment(4);    
		$res=$this->IndexModel->getProductById($id);

		$this->load->view('dashboard/show-factor-detail',array('res'=>$res));
	}

    public function showFactorByUserId(){
         $obj=new Utility();
        $id= $this->uri->segment(4);
        $data=$obj->getWithId('factor',$id,'user_id');
        $this->load->view('dashboard/show-user-factor',array('data'=>$data));

    }
	public function showUserSoldCakes(){
		$obj=new Utility();
		$this->load->helper('date');
		$users=$obj->getAll('users');
		$items=$obj->getWithId('items',null,'price_special !=');
		$sales=$this->IndexModel->getSoldItems();
		
				
		//counting amount of sold products
		$totalProductAmount=[];
		foreach($items as $i){
			$amount=0;
			foreach($sales as $s){ 
				if ($s['p_id']==$i['id']){
					$amount=$amount+$s['f_amount'];
				}
				
				
			}
			$totalProductAmount[]=$amount;
		}
	

		$this->load->view('dashboard/show-sold-cakes',array('users'=>$users,'items'=>$items,'sales'=>$sales,'totalProductAmount'=>$totalProductAmount));
		

	}


	public function getUserSoldViaFilter()
	{
		
			
			$start_d=splitDate($_POST['start_d']);
			$end_d=splitDate($_POST['end_d']);
			$start_t=$this->input->post('start_t');
			$end_t=$this->input->post('end_t');
			$start=$start_d.$start_t;
			$end=$end_d.$end_t;



		$obj=new Utility();
		$users=$obj->getAll('users');
		$items=$obj->getWithId('items',null,'price_special !=');
		$sales=$this->IndexModel->getSoldItems();

		//geting created_at and convert to time stamp
		$obj=new Utility();
			$this->load->helper('date');
			$users=$obj->getAll('users');
			$items=$obj->getWithId('items',null,'price_special !=');
			$sales=$this->IndexModel->getSoldItemsByDate($start,$end);
			
		
				
		//counting amount of sold products
			$totalProductAmount=[];
			
			foreach($items as $i){
				$amount=0;
					foreach($sales as $s){ 
						if ($s['p_id']==$i['id']){
							$amount=$amount+$s['f_amount'];	
						}
					}
			$totalProductAmount[]=$amount;
			
			}


			$this->load->view('dashboard/show-sold-cakes2', array('users'=>$users,'items'=>$items,'sales'=>$sales,'totalProductAmount'=>$totalProductAmount));

	}

	

}