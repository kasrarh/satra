<?php


class User extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portals/Users/UsersModel','UsersModel');
		$this->load->model("AuthModel");
		$this->load->model("IndexModel");
		$this->load->library('Utility');
		$this->load->database();
		$this->load->library('AuthLib');
		if ($_SESSION['role'] == 'user'){

		}else{
			redirect(base_url('Auth/index'));
		}
//		$User = new AuthLib();
//		$token = $User->getToken('users');
//		$User->checkToken($token, $_SESSION['token'], 'Auth/LoginUser');
//		$User->isLogin('isUser', 'Auth/LoginUser');
//		$image = $User->GetPhotoBySession('admin', 'image_url');
	}

	public function showUserFactor()
	{
		$obj=new Utility();
		$auth= new AuthLib();
		$id=$auth->GetIdBySession('users','user_id');
		$factors=$this->UsersModel->getUserFactors($id);
		$this->load->view('user/show-user-factor',array('factors'=>$factors));

	}
	public function showCustomerFactor()
	{
		$obj=new Utility();
		$auth= new AuthLib();
		$id=$auth->GetIdBySession('customers','customer_id');
		$factors=$obj->getWithId('factor',$id,'customer_id');
		$this->load->view('user/show-user-factor',array('factors'=>$factors));

	}
	public function showFactorDetail()
	{
		$id= $this->uri->segment(4);
		$res=$this->IndexModel->getProductById($id);
		$this->load->view('user/show-factor-detail',array('res'=>$res));
	}

}


//	public function addSubCategory()
//	{
//		$obj= new Utility();
//		$category=$obj->getAll('category');
//
//		$this->load->view('dashboard/add-sub-category',array('category'=>$category));
//		if (isset($_POST['submit'])){
//			$img=$obj->uploadFile2('uploads/category/','files');
//			$data=array(
//				'name'=>$_POST['name'],
//				'en_name'=>$_POST['en_name'],
//				'category_id'=>$_POST['category_id'],
//				'image_url'=>$img,
//
//			);
//			$res=$this->IndexModel->add('category2',$data);
//			if ($res){
//				redirect(base_url('Dashboard/Show/showSubCategory/'.$_POST['category_id']));
//			}
//		}
//	}
//	public function editSubCategory()
//	{
//		$id = $this->uri->segment(4);
//		$obj = new Utility();
//		$category = $obj->getAll('category');
//		$data2=$obj->getWithId('category2',$id,'category2_id');
//
//		$this->load->view('dashboard/edit-sub-category', array('category' => $category,'data2'=>$data2));
//		if (isset($_POST['submit'])) {
//				$img = $obj->uploadFile2('uploads/category/', 'files');
//			die(var_dump($img));
//			$data = array(
//				'name' => $_POST['name'],
//				'en_name' => $_POST['en_name'],
//				'category_id'=>$_POST['category_id'],
//				'image_url' => $img,
//
//
//			);
//			if ($img==null){
//				unset($data['image_url']);
//			}
//						die(var_dump($img));
//
//			$res = $this->IndexModel->edit('category2', $id, 'category2_id', $data);
//			if ($res) {
//				redirect(base_url('Dashboard/Show/showSubCategory/'.$data['category_id']));
//			}
//		}
//	}
//
//	public function showSubCategory()
//	{
//		$id= $this->uri->segment(4);
//		$obj= new Utility();
//		$data1=$obj->getWithId('category',$id,'category_id');
//		$data2=$obj->getWithId('category2',$id,'category_id');
//		die(var_dump($data2));
//
//		$this->load->view('dashboard/show-sub',array( 'data1'=>$data1,'data2'=>$data2));
//
//	}
//
//	public function deleteSubCategory()
//	{
//		$id= $this->uri->segment(4);
//		$obj= new Utility();
//		$category2=$obj->delete(base_url('Dashboard/Show/showSubCategory/'),'category2','category2_id',$id);
//
//
//	}
