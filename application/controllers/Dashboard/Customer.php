<?php


class Customer extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portals/Users/UsersModel','UsersModel');
		$this->load->model("AuthModel");
		$this->load->library('Utility');
		$this->load->database();
		$this->load->library('AuthLib');
		$User = new AuthLib();
		if ($_SESSION['role'] == 'customer'){

		}else{
			redirect(base_url('Auth/index'));
		}
//		$token = $User->getToken('admin');
//		$User->checkToken($token, $_SESSION['token'], 'Auth/LoginAdmin');
//		$User->isLogin('isAdmin', 'Auth/LoginAdmin');
//		$image = $User->GetPhotoBySession('admin', 'image_url');
	}

	public function showCustomerFactor()
	{
		$obj=new Utility();
		$auth= new AuthLib();
		$id=$auth->GetIdBySession('customers','customer_id');
		$factors=$this->UsersModel->getCustomerFactors($id);
		$this->load->view('user/show-user-factor',array('factors'=>$factors));

	}
	public function showFactorDetail()
	{
		$id= $this->uri->segment(4);
		$obj= new Utility();
		$data=$obj->getWithId('factor_detail',$id,'factor_id');
		$this->load->view('dashboard/show-factor-detail',array( 'data'=>$data));
	}


}
