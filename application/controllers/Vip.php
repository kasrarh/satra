<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Vip extends CI_Controller
{



	public function __construct()
	{
		parent::__construct();
		$this->load->model('Portals/Admins/AdminsModel', "AdminsModel");
		$this->load->model('IndexModel', "IndexModel");
		$this->load->Library('Utility');
		if(!isset($_SESSION['role']) and $_SESSION['role']!='user'){
            redirect(base_url('Auth/index'));
        }

	}


	public function index()
	{
		$obj = new Utility();
		$category1 = $obj->getAll('category');
		$category2 = $this->AdminsModel->getCategory2();
		$items = $this->AdminsModel->getItems();
		$this->load->view('vip', array('category2' => $category2, 'items' => $items));
	}


}
