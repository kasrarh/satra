<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Shop extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->model('ShopModel', "ShopModel");
		$this->load->model('AuthModel', "AuthModel");
		$this->load->database();
		$this->load->library('Utility');
		$this->load->library('AuthLib');
		$this->load->library('Pay');
	}

	public function addProductToCart()
	{
		$obj=new Utility();
		$item_id = $this->input->get('id');
		$packageDetail=$obj->getWithId('items',$item_id,'id');
//		$customer_id = $this->AuthModel->GetCustomerIdBySession();
		$customer_id = 1;
		$shoppingCart = $this->ShopModel->getProductsFromShoppingCartByCustomerId($customer_id);
		$dontAdd = false;
		foreach ($shoppingCart as $product){
			if ($product['product_id'] == $item_id){
				$this->ShopModel->addCount($product['shopping_cart_id']);
				$dontAdd = true;
			}
		}
		if (!$dontAdd){
			$data = array(
				'customer_id' => $customer_id,
				'price' => $packageDetail[0]['price_normal'],
				'product_id' => $item_id,
			);
			$this->ShopModel->addPackageToCart($data);
		}
//		if ($data) {
//			echo json_encode($price);
//		}
	}
	public function deleteProductFromCart()
	{
		$obj=new Utility();
		$item_id = $this->input->get('id');
		$packageDetail=$obj->getWithId('items',$item_id,'id');
//		$customer_id = $this->AuthModel->GetCustomerIdBySession();
		$customer_id = 1;
		$shoppingCart = $this->ShopModel->getProductsFromShoppingCartByCustomerId($customer_id);
		$deleteRow = true;
		foreach ($shoppingCart as $product){
			if ($product['product_id']== $item_id and $product['count'] > 1){
				$this->ShopModel->decreseCount($product['shopping_cart_id']);
				$deleteRow = false;
			}
		}
		if ($deleteRow){
			$this->ShopModel->deletePackageFromCart($product['shopping_cart_id']);
		}
//		if ($data) {
//			echo json_encode($price);
//		}
	}

	public function Shop()
	{
		$Pay = new Pay();
		$price = $_SESSION['TotalPrice'];
		$Pay->PayRequest($price,'Shop/PayVerify');
	}
	public function PayVerify()
	{
		$Amount=$_SESSION['TotalPrice'];
		$Authority = $_GET['Authority'];
		$data = array('MerchantID' => 'a6e4946d-0caa-4a96-bf7b-f6ac45d39741', 'Authority' => $Authority, 'Amount' => $Amount);
		$jsonData = json_encode($data);
		$ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/PaymentVerification.json');
		curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($jsonData)
		));
		$result = curl_exec($ch);
		$err = curl_error($ch);
		curl_close($ch);
		$result = json_decode($result, true);
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			if ($result['Status'] == 100) {
				$tracking_code = $result['RefID'];
				$factorID = $_SESSION['factor_id'];
				$this->ShopModel->UpdateFactorStatus($factorID);
				unset($_SESSION['factor_id']);
				unset($_SESSION['TotalPrice']);
				$this->load->view('dashboard/payment', array("tracking_code" => $factorID, "backgroundColor" => '#1b8c51', "Status" => 'پرداخت موفق'));
			} else {

				$this->load->view('dashboard/payment', array("backgroundColor" => '#92122a', "Status" => 'پرداخت ناموفق'));
			}
		}
	}
	public function CountShoppingCart()
	{
		$obj=new Utility();
//		$customer_id = $this->AuthModel->GetCustomerIdBySession();
		$customer_id = 1;
		$count  = $this->ShopModel->totalCountShoppingCart($customer_id);
		if ($count) {
			echo json_encode($count->count);
		}
	}
}
