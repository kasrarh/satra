<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Auth extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		error_reporting(0);
		$this->load->helper('string');
		$this->load->model("AuthModel");
		$this->load->library("Utility");
		$this->load->library("AuthLib");

	}

	public function index()
	{
		$obj = new Utility();
	
		$this->load->view('login');


		if (isset($_POST['login-submit']) and isset($_POST['user-name']) and isset($_POST['user-password'])) {

			$username = $_POST['user-name'];
			$password = $this->AuthModel->hash($_POST['user-password']);
			$check = $this->AuthModel->findPerson($username, $password,'customers');
			$check2 = $this->AuthModel->findPerson($username, $password,'users');
			$checkAdmin = $this->AuthModel->findPerson($username, $password,'admin');

			if ($check) {
				$this->AuthModel->doLogin($username, $password);
				$_SESSION['role']='customer';
				$_SESSION['isCustomer']=true;
				redirect(base_url('Show/index'));
			} elseif ($check2) {
				$this->AuthModel->doLogin($username, $password);
				$_SESSION['role']='user';
				$_SESSION['isUser']=true;
				redirect(base_url('Show/index'));

			} elseif ($checkAdmin) {
				$_SESSION['role']='admin';
				$_SESSION['is_admin']=true;
				$this->AuthModel->doLogin($username, $password);
				redirect(base_url('Dashboard/Show/addCategory'));

			}else{
				$_SESSION['error_login'] = true;
				redirect(base_url('Auth/index'));
			}

		}


	}

	public function studentVerification($id)
	{
		$code = $this->generateRandomNumber();
		$data = array(
			'student_id' => $id,
			'code' => $code
		);
		//need to send the code via email or phone number
		$info = $this->db->insert('student_verify_code', $data);
		if ($info == true) {
			$obj = new Utility();
			$FakeCustomer = $this->AuthModel->getCustomerFake($_SESSION['fake_id']);
			$obj->multiSms(array($FakeCustomer->phone), 'کد احراز حویت شما:' . $code);
			redirect(base_url('Auth/checkVerificationCode'));
		} else {
			base_url(base_url('Auth/index'));
		}

	}

	public function CustomerSubmit()
	{
		$this->load->view('register');
		if (isset($_POST['submit'])) {
			if ($this->input->post('password') == $this->input->post('passwordConfirm')) {
				$user = new AuthLib();
				$username = $this->input->post('username');
				$email = $this->input->post('email');
				$password = $user->hash($_POST['password']);
				$data = array(
					"username" => $username,
					"password" => $user->hash($password),
					"email" => $email,
					"phone" => $_POST['phone'],
					"name" => $_POST['fullname']
				);
				$result = $user->findDuplicate('customers', 'username', $username);

				if ($user->findDuplicate('customers', 'username', $username) and $user->findDuplicate('customers', 'email', $email) == true) {
					$res = $this->db->insert('customers', $data);
//					$id = $this->db->insert_id();
//					if ($res) {
////						$_SESSION['fake_id'] = $id;
////						$this->studentVerification($id);
//					} else {
//						$_SESSION['error'] = 'خطایی در هنگام ثبت رخ داده است! دوباره تلاش کنید.';
//						redirect('Auth/CustomerSubmit');
//					}
				} else {
					$_SESSION['error'] = 'کاربر با این ایمیل یا نام کاربری موجود است!';
					redirect('Auth/CustomerSubmit');
				}
			} else {
				$_SESSION['error'] = 'پسورد‌ها یکدیگر را تایید نمی‌کنند!';
				redirect('Auth/CustomerSubmit');
			}
		}
	}
		public
		function LoginAdmin()
		{
		
			$this->load->view('login');
			if (isset($_POST['login-submit']) and isset($_POST['user-name']) and isset($_POST['user-password'])) {
				$user = new AuthLib();
				$username = $_POST['user-name'];
				$password = $user->hash($_POST['user-password']);
				$user->login($username, $password, 'admin', 'isAdmin','admin_id',base_url('Dashboard/Show/addCategory'), 'Auth/LoginAdmin');
			}
		}
	public
	function LoginCustomer()
	{
		$this->load->view('login');
		if (isset($_POST['login-submit']) and isset($_POST['user-name']) and isset($_POST['user-password'])) {
			$user = new AuthLib();
			$username = $_POST['user-name'];
			$password = $user->hash($_POST['user-password']);

			$user->login($username, $password, 'customers', 'isCustomer','customer_id',base_url('Dashboard/Customer/showCustomerFactor'), 'Auth/LoginCustomer');
		}
	}
	public
	function LoginUser()
	{
		$this->load->view('login');
		if (isset($_POST['login-submit']) and isset($_POST['user-name']) and isset($_POST['user-password'])) {
			$user = new AuthLib();
			$username = $_POST['user-name'];
			$password = $user->hash($_POST['user-password']);
			$user->login($username, $password, 'users', 'isUser','user_id',base_url('Dashboard/User/showUserFactor'), 'Auth/LoginUser');
		}
	}


		public
		function checkVerificationCode()
		{
			$this->load->view('dashboard-portals/student-verification-code');
			if (isset($_POST['submit'])) {
				$code = $_POST['verifyNumber'];
				$date = $this->AuthModel->getTime($code);
				if ($date != null) {
					$currentTime = date("Y-m-d h:i:s");
					$res = strtotime($currentTime) - strtotime($date[0]['create_at']);

					if ($res <= 36000) {
						$result = $this->AuthModel->deleteVerificationCode2($code, $_SESSION['fake_id']);
						if ($result == true) {
							$giftCode = $this->generateGiftCode();
							$FakeCustomer = $this->AuthModel->getCustomerFake($_SESSION['fake_id']);
							if ($FakeCustomer) {
								$data = array('gift_code' => $giftCode, 'fullname' => $FakeCustomer->fullname, 'password' => $FakeCustomer->password, 'phone' => $FakeCustomer->phone, 'email' => $FakeCustomer->email, 'username' => $FakeCustomer->username);
								$addResult = $this->AuthModel->addCustomer($data);
								$deleteResult = $this->AuthModel->deleteCustomerFake($_SESSION['fake_id']);
								if ($addResult and $deleteResult) {
									$obj = new Utility();
									$user = new AuthLib();
									$welcome_text = $FakeCustomer->fullname . ' ' . 'عزیر ثبت نام شما با موفقیت تکمیل شد و کیف پول شما به عنوان هدیه مبلغ 30,000 تومان شارژ شد.';
									$obj->multiSms(array($FakeCustomer->phone), $welcome_text);
									$user->login($FakeCustomer->username, $FakeCustomer->password, 'customer', 'isCustomer', 'Show/index', 'Auth/index');

								}
							} else {
								$_SESSION['error_message'] = 'کاربری با این نام ثبت نام نشده است!';
								redirect(base_url('Auth/CustomerSubmit'));

							}

						}

					} else {
						$result = $this->AuthModel->deleteVerificationCode($code);
						if ($result == true) {
							$this->studentVerification($_SESSION['fake_id']);

						}
					}
				} else {
					$_SESSION['error_verification'] = ' کد وارد شده با کد ارسالی شده مطابقت ندارد.';
					redirect(base_url('Auth/checkVerificationCode'));

				}
			}

		}

		function generateRandomNumber($length = 8)
		{
			$characters = '0123456789';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}

		function generateGiftCode($length = 10)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$charactersLength = strlen($characters);
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, $charactersLength - 1)];
			}
			return $randomString;
		}

		public
		function Logout()
		{
			session_unset();
			redirect(base_url('Show/index'));
		}

		public
		function GoToDashboard()
		{
			$username = $_SESSION['username'];
			$user = $this->AuthModel->findUsername($username);
			$admin = $this->AuthModel->findUsernameAdmin($username);
			if ($user) {
				redirect(base_url('Portals/Students/Show/index'));
			} elseif ($admin) {
				redirect(base_url('Portals/Admin/Admin'));
			} else {
				redirect(base_url('Auth/LoginCustomer'));
			}
		}

		public
		function LogoutAdmin()
		{
			$this->AuthModel->doLogoutAdmin();
			redirect(base_url('Show/index'));
		}

		public
		function findUsername()
		{
			$username = $this->input->get('username');
			$table = $this->input->get('table');
			$field = 'username';
			$SameUser = new AuthLib();
			if (
			$SameUser->findDuplicate($table, $field, $username)
			) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		}

		public
		function findUsernameEdit()
		{
			$username = $this->input->get('username');
			$SameUser = new AuthLib();
			if (
			$SameUser->findDuplicateUsername($username, true)
			) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		}

		public
		function findEmail()
		{
			$email = $this->input->get('email');
			$SameUser = new AuthLib();
			if (
			$SameUser->findDuplicateEmail($email)
			) {
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}
		}

		public
		function ResetPassword()
		{
			$this->load->view('reset-password', array());

			if (isset($_POST['emailSubmit'])) {
				$email = $this->input->post('email');
				$token = $this->random_string('alnum', 6);
				$data = $this->AuthModel->findCustomerEmail($email);

				if ($data) {
					$this->AuthModel->UpdateTokenWithEmail($token, $email);
					$From = "info@eamirkabir.com";
					$To = $email;
					$Subject = "Reset Password";
					$Body = '
<html>
<head>
	<style>
body {
    padding: 3rem 1rem;
			background-color: #ffc107;
			background-image: url(/images/background-pattern.png);
		}

		.panel {
    max-width: 40rem;
			margin: 1rem auto;
			padding: 3rem;
			background-color: #ffffff;
			border-radius: .25rem;
		}

		h1, h2 {
    line-height: 1.2;
		}

		h1 {
    font-weight: 300;
			font-size: 3.5rem;
			margin-bottom: 3rem;
		}

		h2 {
    font-weight: 400;
			font-size: 1.5rem;
			margin-bottom: 1rem;
			margin-top: 3rem;
		}

		p {
    margin: 1rem 0;
			padding: 0 1.5rem;
		}

		b {
    font-weight: 600;
		}
	</style>
</head>
<body>
	<div class=\"panel\">
		<h1 style=\"text-align: center;\">تغییر رمز عبور</h1>


		<h2>فقط کافیه روی این <a style="color: #00a2e3" href="' . base_url('Auth/ResetPasswordMain') . '?token=' . $token . '&email=' . $email . '" >لینک</a> کلیک کنید.</h2>
	</div>
</body>
</html>';

					SendMail($From, $To, $Subject, $Body);
					$_SESSION['success'] = true;
					redirect(base_url('Auth/ResetPassword'));
				} else {
					$_SESSION['error'] = true;
					redirect(base_url('Auth/ResetPassword'));
				}
			}
		}

		function random_string($type = 'alnum', $len = 8)
		{
			switch ($type) {
				case 'basic':
					return mt_rand();
				case 'alnum':
				case 'numeric':
				case 'nozero':
				case 'alpha':
					switch ($type) {
						case 'alpha':
							$pool = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
							break;
						case 'alnum':
							$pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
							break;
						case 'numeric':
							$pool = '0123456789';
							break;
						case 'nozero':
							$pool = '123456789';
							break;
					}
					return substr(str_shuffle(str_repeat($pool, ceil($len / strlen($pool)))), 0, $len);
				case 'unique': // todo: remove in 3.1+
				case 'md5':
					return md5(uniqid(mt_rand()));
				case 'encrypt': // todo: remove in 3.1+
				case 'sha1':
					return sha1(uniqid(mt_rand(), TRUE));
			}
		}

		public
		function ResetPasswordMain()
		{
			$token = $this->input->get('token');
			$email = $this->input->get('email');
			$data = $this->AuthModel->findCustomerByTokenAndEmail($token, $email);
			if ($data) {
				$this->load->view('reset-password-main', array());
				if (isset($_POST['newPassSubmit'])) {
					$newPassword = $this->input->post('newPassword');
					$id = $data->customer_id;
					$data2 = $this->AuthModel->UpdatePassword($id, $newPassword);
					if ($data2) {
						$_SESSION['success_newPass'] = true;
						sleep(3);
						redirect(base_url('Auth/index'));
					} else {
						$_SESSION['error_newPass'] = true;
					}
				}
			}
		}
	}

