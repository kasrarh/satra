<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Contact extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('ContactModel','ContactModel');


	}

	public function index()
	{
		$this->load->view('contact');
		if (isset($_POST['submit'])){
			$contactName=$_POST['contactName'];
			$contactEmail= $_POST['contactEmail'];
			$contactText=$_POST['contactText'];

			$data=array(
			'name'=> $contactName,
			'email'=>$contactEmail,
			'text'=>$contactText
			);
			$this->ContactModel->addContact($data);
		}

	}


}

