<?php
class UsersModel extends CI_Model{

    public function getCustomerFactors($customerId){
        $this->db->select('*');
        $this->db->from('factor');
        $this->db->where('customer_id',$customerId);
        $this->db->where('status',1);
        $res=$this->db->get()->result_array();
        return $res;
    }
    public function getUserFactors($userId){
        $this->db->select('*');
        $this->db->from('factor');
        $this->db->where('user_id',$userId);
        $this->db->where('status',1);
        $res=$this->db->get()->result_array();
        return $res;
    }
}