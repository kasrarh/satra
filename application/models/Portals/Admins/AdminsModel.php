<?php

class AdminsModel extends CI_Model
{
	public function getSeminars($teacher_id)
	{
		$data = $this->db->get_where('seminar', array("teacher_id" => $teacher_id))->result_array();

		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getClasses()
	{
		$data = $this->db->get('class')->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}
	public function getPackages()
	{
		$data = $this->db->get('product_package')->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getDepartmentByID($teacher_id)
	{
		$data = $this->db->get_where('teachers', array("id" => $teacher_id))->row();

		if ($data != null) {
			return $data->department_id;
		} else return false;
	}

	public function getInfos($package_id)
	{
		$data = $this->db->get_where('teacher_and_buyers', array("package_id" => $package_id))->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getInfosClasses($class_id)
	{
		$data = $this->db->get_where('teacher_and_classes', array("class_id" => $class_id))->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getExams($class_id)
	{
		$data = $this->db->get_where('exams', array("class_id" => $class_id))->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getMessages($admin_id)
	{
		$this->db->from('messages');
		$this->db->where("admin_id", $admin_id);
		$this->db->join('teachers', 'teachers.id =messages.teacher_id');
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}
	}

	public function getStudentMessages()
	{
		$this->db->select('*,teachers.fullname as teacher_fullname,messages.id as message_id');
		$this->db->from('messages');
		$this->db->where("status", null);
		$this->db->join('teachers', 'teachers.id =messages.teacher_id');
		$this->db->join('customer', 'customer.customer_id =messages.customer_id');
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}
	}
	public function getStudentMessagesTotal()
	{
		$this->db->select('*,teachers.fullname as teacher_fullname,messages.id as message_id');
		$this->db->from('messages');
		$this->db->join('teachers', 'teachers.id =messages.teacher_id');
		$this->db->join('customer', 'customer.customer_id =messages.customer_id');
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}
	}
	public function getExam($exam_id)
	{
		$data = $this->db->get_where('exams', array("id" => $exam_id))->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getAdmins()
	{
		$data = $this->db->get('admin')->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getQuestions($exam_id)
	{
		$data = $this->db->get_where('exam_questions', array("exam_id" => $exam_id))->result_array();
		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getProducts($package_id)
	{
		$data = $this->db->get_where('products', array("product_package_id" => $package_id))->result_array();

		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getProduct($product_id)
	{
		$data = $this->db->get_where('products', array("products_id" => $product_id))->result_array();

		if ($data != null) {
			return $data;
		} else return false;
	}

	public function getQuestion($question_id)
	{
		$data = $this->db->get_where('exam_questions', array("id" => $question_id))->result_array();

		if ($data != null) {
			return $data;
		} else return false;
	}

	public function setPresent($student_id, $class_id)
	{
		$this->db->set('status', 1);
		$this->db->where('customer_id', $student_id);
		$this->db->where('seminar_id', $class_id);
		$result = $this->db->update('presency');
		if ($result) {
			return true;
		} else return false;
	}

	public function sendMessage($title, $class, $message, $jalaliDate, $student, $admin, $teacher_id)
	{
		$data = array(
			"customer_id" => $student,
			"admin_id" => $admin,
			"class_id" => $class,
			"title" => $title,
			"message" => $message,
			"created_at" => $jalaliDate,
			"teacher_id" => $teacher_id
		);
		$res = $this->db->insert('messages', $data);
		if ($res == true) {
			return true;
		}
	}

	public function getStudents($seminar_id)
	{
		$this->db->from('presency');
		$this->db->where("seminar_id", $seminar_id);
		$this->db->join('customer', 'customer.customer_id =presency.customer_id');


		$data = $this->db->get()->result_array();


		if ($data) {
			return $data;
		}
	}

	public function getClassMembers($class_id)
	{
		$this->db->from('class_members');
		$this->db->where("class_id", $class_id);
		$this->db->join('customer', 'customer.customer_id =class_members.student_id');
		$data = $this->db->get()->result_array();


		if ($data) {
			return $data;
		}
	}
	public function setPublishMessage($id)
	{
		$this->db->set('status',1);
		$this->db->where('id',$id);
		$res=$this->db->update('messages');
		if ($res){
			return true;
		}else return false;
	}
	public function setPublishInfo($id,$table)
	{
		$this->db->set('status',2);
		$this->db->where('id',$id);
		$res=$this->db->update($table);
		if ($res){
			return true;
		}else return false;
	}

	public function getOrdersByCustomerId($id)
	{
		$this->db->select('*, orders.product_id as o_product_id');
		$this->db->from('orders');
		$this->db->where('customer_id',$id);
		$this->db->join('products','products.product_id=orders.product_id');
		$res=$this->db->get()->result_array();
		if ($res){
			return $res;
		}

	}

	public function approveOrder($id)
	{
		$this->db->where('id',$id);
		$this->db->update('orders',array('status'=>1));

	}

	public function deleteOrder($id)
	{
		$this->db->where('id',$id);
		$this->db->delete('orders');

	}

	public function getCategory2()
	{
		$this->db->select('*,category.name as name2,category.image_url as img');
		$this->db->from('category');
		$res=$this->db->get()->result_array();
		if ($res){
			return $res;
		}

	}
	public function getCategory2en()
	{
		$this->db->select('*,category2.en_name as name2,category1.en_name as name1,category2.image_url as img');
		$this->db->from('category2');
		$this->db->join('category1','category1.category1_id=category2.category1_id');
		$res=$this->db->get()->result_array();
		if ($res){
			return $res;
		}

	}
	public function getItems()
	{
		$this->db->select('*,category.name as name2,items.name as i_name,items.image_url as img,items.id as i_id');
		$this->db->from('items');
		$this->db->join('category','category.category_id=items.category2_id');
		$res=$this->db->get()->result_array();
		if ($res){
			return $res;
		}

	}
	public function getItemsEn()
	{
		$this->db->select('*,category2.en_name as name2,items.en_name as i_name,items.image_url as img,items.id as i_id');
		$this->db->from('items');
		$this->db->join('category2','category2.category2_id=items.category2_id');
		$res=$this->db->get()->result_array();
		if ($res){
			return $res;
		}

	}
}
