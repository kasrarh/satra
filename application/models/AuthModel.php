<?php
class AuthModel extends CI_Model
{
    public function find($username, $pass)
    {
        $data = $this->db->get_where('users', array("username" => $username, "password" => $pass))->row();
      
      
        if ($data!=null) {
            return true;
        } else return false;
    }


    public function UpdateToken($token, $username)
    {
        $this->db->set('token', $token);
        $this->db->where('username', $username);
        $result = $this->db->update('users');
        if ($result) {
            return true;
        } else return false;
    }

    public function doLogin($username, $password)
    {
        $_SESSION["username"] = $username;
        $_SESSION["password"] = $password;
        $role=$this->db->get_where('users',array("username"=>$username,"password"=>$password))->row()->role;

        // $_SESSION["role"] = $role;
        //generate simple token:

        $token = random_string('alnum', 10);
        $_SESSION["token"] = $token;
        $this->UpdateToken($token, $username);
        return $role;
        
    }

    public function doLogout()
    {
        unset($_SESSION['username'], $_SESSION['password'], $_SERVER['REMOTE_ADDR'], $_SESSION['token']);
        return true;
    }

    public function isAdmin()
    {
        if (isset($_SESSION['username'])&& $_SESSION['role']=='admin')
            return true;
    }

    public function hash($password)
    {
        $saltStr = 'yellowbeauty@139004';
        $hash = sha1($saltStr . md5($password . $saltStr));
        return $hash;
    }

	public function findPerson($username,$password,$table)
	{
		$res=$this->db->get_where($table,array('username'=>$username,'password'=>$password))->result_array();
		if ($res){
			return true;
		}else return  false;
    }
}
