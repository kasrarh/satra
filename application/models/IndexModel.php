<?php

class IndexModel extends CI_Model{




	public function add($table,$info)
	{
		$res=$this->db->insert($table,$info);
		if ($res){
			return true;
		}
	}

	public function edit($table,$id,$field,$info)
	{
		$this->db->where($field,$id);
		$res=$this->db->update($table,$info);
		if ($res){
			return true;
		}

	}
	public function delete($id,$table){
        $this->db->where('id',$id);
        $result=$this->db->delete($table);
        if($result){
            return true;
        }
    }
	
	// public function getShoppingCartById($isUser=false,$isCustomer=false,$id){
	// 	$this->load->Library('AuthLib');
	// 	$user=new AuthLib();
	// 	$this->db->select('*');
	// 	$this->db->from('shopping_cart');
	// 	if($isUser==true){
	// 		$this->db->where('user_id',$id);
	// 		$this->db->join('items','shopping_cart.product_id=items.id');
	// 		$res=$this->db->get()->result_array();
	// 		return $res;
	// 	}
	// 	if($isCustomer==true){
	// 		$this->db->where('customer_id',$id);
	// 		$this->db->join('items','shopping_cart.product_id=items.id');
	// 		$res=$this->db->get()->result_array();
	// 		return $res;
	// 	}
	// }



	public function getProductById($id)
	{
		$this->db->select('*');
		$this->db->from('factor_detail');
		$this->db->where("factor_detail.factor_id" , $id);
// 		$this->db->join('factor',"factor_detail.factor_id=factor.factor_id");
		$this->db->join('items', 'items.id =factor_detail.product_id');
		$data = $this->db->get()->result_array();


		if ($data) {
			return $data;
		}

	}
	public function getLatestFactorId(){
		$last_row=$this->db->select('factor_id as f_id')->order_by('factor_id',"desc")->limit(1)->get('factor')->row()->f_id;
		return $last_row;
	}
	public function getLatestCustomer(){
		$last_row=$this->db->select('customer_id')->order_by('customer_id',"desc")->limit(1)->get('customers')->row()->customer_id;
		return $last_row;
	}
	public function getSoldItems(){
		$this->db->select('factor.* , factor_detail.product_id as p_id , factor_detail.amount as f_amount');
		$this->db->from('factor');
		$this->db->where('factor.customer_id',null);
		$this->db->where('factor.status',1);
		$this->db->join('factor_detail','factor_detail.factor_id=factor.factor_id');
		$res=$this->db->get()->result_array();
		if($res){
			return $res;
		}
	}
	public function getSoldItemsByDate($start,$end){
		$this->db->select('factor.* , factor_detail.product_id as p_id , factor_detail.amount as f_amount');
		$this->db->from('factor');
		$this->db->where('factor.customer_id',null);
		$this->db->where('factor.status',1);
		$this->db->where('factor.created_at>=',$start);
		$this->db->where('factor.created_at<=',$end);
		$this->db->join('factor_detail','factor_detail.factor_id=factor.factor_id');
		$res=$this->db->get()->result_array();
		if($res){
			return $res;
		}
	}
	public function getCustomerFactor($end,$start){
		$this->db->select('factor.* , customers.name , customers.phone,factor.customer_id as c_id , factor.created_at as factor_date , factor.address as f_address');
		$this->db->from('factor');
		$this->db->where('factor.created_at <=',$end);
		$this->db->where('factor.created_at >=',$start);
		$this->db->where('factor.status',1);
		$this->db->join('customers','customers.customer_id=factor.customer_id');
		$res=$this->db->get()->result_array();
		if($res){
			return $res;
		}
	}
	public function getUserFactor($end,$start){
		$this->db->select('factor.* , users.store_name , factor.created_at as factor_date , factor.address as f_address');
		$this->db->from('factor');
		$this->db->where('factor.created_at <=',$end);
		$this->db->where('factor.created_at >=',$start);
		$this->db->where('factor.status',1);
		$this->db->join('users','users.user_id=factor.user_id');
		$res=$this->db->get()->result_array();
		if($res){
			return $res;
		}
	}
}
	
	
	
