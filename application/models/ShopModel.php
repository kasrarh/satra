<?php

class ShopModel extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function ShowPackageProducts($pageNumber)
    {
		$offset= ($pageNumber-1)*12;
		$this->db->limit(12, $offset);
        $data = $this->db->get('product_package')->result_array();
        if ($data) {
            return $data;
        }
    }
	public function GetAllPackages()
	{
		$data = $this->db->get('product_package')->result_array();
		if ($data) {
			return $data;
		}
	}
	public function getWallet($id)
	{
		$data = $this->db->get_where('customer', array('customer_id' => $id ))->row();
		if ($data) {
			return $data->wallet;
		}
	}
	public function BestSellerPackages()
	{
		$this->db->select('*');
		$this->db->order_by('selling_rate', 'DESC');
		$data = $this->db->get('product_package')->result_array();
		if ($data) {
			return $data;
		}
	}
	public function CheapestPackages()
	{
		$this->db->select('*');
		$this->db->order_by('total_price', 'ASC');
		$data = $this->db->get('product_package')->result_array();
		if ($data) {
			return $data;
		}
	}
	public function MostViewedPackages()
	{
		$this->db->select('*');
		$this->db->order_by('view_rate', 'DESC');
		$data = $this->db->get('product_package')->result_array();
		if ($data) {
			return $data;
		}
	}
	public function addRateByID($id)
	{
		$this->db->set('view_rate', 'view_rate+1' , false);
		$this->db->where('package_id', $id);
		$data = $this->db->update('product_package');
		if ($data) {
			return true;
		}
	}
    public function addGiftCodeToFactor($factorId,$code)
    {
        $this->db->set('gift_code', $code);
        $this->db->where('id', $factorId);
        $data = $this->db->update('factor');
        if ($data) {
            return true;
        }
    }

    public function updateGiftCodeCounter($code)
    {
        $this->db->set('gift_code_counter', 'gift_code_counter+1' , false);
        $this->db->where('gift_code', $code);
        $data = $this->db->update('customer');
        if ($data) {
            return true;
        }
    }

    public function checkGiftCodeByCustomerId($code)
    {
        $this->db->get_where('customer',array('gift_code'=>$code))->result_array();

    }
    public function getFactorById($id)
    {
        $res=$this->db->get_where('factor',array('id'=>$id))->result_array();
        if ($res){
            return $res;
        }
    }
	public function NewFactor($id,$FinalPrice,$date,$obstruct_code,$totalDiscount,$netPrice)
	{
		$data = array(
			'customer_id' => $id,
			'total_price' => $FinalPrice,
			'created_at'=>$date,
			'net_price'=>$netPrice,
			'discount'=>$totalDiscount,
			'obstruct_code' =>$obstruct_code
		);
		$this->db->insert('factor', $data);
	}
	public function GetFactorId($obstruct_code)
	{
		$data = $this->db->get_where('factor',array("obstruct_code"=>$obstruct_code))->row();
		if ($data) {
			return $data->id;
		}else return false;
	}
	public function addProductToFactor($product,$id,$factorId,$downloadable=0)
	{
		$data = array(
			'customer_id' => $id,
			'product_id' => $product['products_id'],
			'factor_id' => $factorId,
			'downloadable' => $downloadable
		);
		$this->db->insert('bought_product', $data);
	}
	public function addPackageToFactor($package,$id,$factorId,$machine_code)
	{
		$data = array(
			'customer_id' => $id,
			'package_id' => $package['package_id'],
			'net_price' => $package['net_price'],
			'price' => $package['price'],
			'factor_id' => $factorId,
			'machine_code' => $machine_code,
			'downloadable'=>1
		);
		$this->db->insert('bought_package', $data);
	}
	public function addCustomerDetails($data,$id)
	{
		$this->db->where('customer_id',$id);
		$data=$this->db->update('customer', $data);
	}
	public function GetProductDetail($id)
	{
		$this->db->select('*, products.duration as p_duration');
		$data = $this->db->get_where('products',array('product_package_id'=>$id))->result_array();

		if ($data) {
			return $data;
		}
	}
	public function GetPackageDepartment($departmentId)
	{
		$this->db->limit(3);
		$this->db->from('product_package');
		$this->db->where("package_id" , $departmentId);
		$this->db->join('department', 'department.id =product_package.department_id');
		$data = $this->db->get()->result_array();


		if ($data) {
			return $data;
		}
	}
	public function GetRandomActivationCode($package_id,$customer_id,$date)
	{
		$this->db->order_by('rand()');
		$this->db->where("status" ,0);
		$data = $this->db->get('serial_numbers')->row();
		$this->db->where('serial_id', $data->serial_id);
		$this->db->set('status', 1);
		$this->db->set('customer_id', $customer_id);
		$this->db->set('package_id', $package_id);
		$this->db->set('used_at', $date);
		$result = $this->db->update('serial_numbers');
		if ($result) {
			return $data->code;
		} else return false;
	}
	public function GetPackageDetail($id)
	{
		$this->db->select('*,teachers.image_url as t_image');
		$this->db->from('product_package');
		$this->db->where('package_id',$id);
		$this->db->join('teachers','teachers.id=product_package.teacher_id');
		$data=$this->db->get()->result_array();
		if ($data) {
			return $data;
		}
	}
	public function GetPackagefromFactor($factor_id,$customer_id)
	{
		$this->db->select('product_package.total_price as p_price,product_package.package_name as p_name,product_package.discount as p_discount');
		$this->db->from('bought_package');
		$this->db->where("bought_package.customer_id" , $customer_id);
		$this->db->join('factor', 'factor.id =bought_package.factor_id');
		$this->db->join('product_package', 'product_package.package_id =bought_package.package_id');
		$data = $this->db->get()->result_array();



		if ($data) {
			return $data;
		}
	}


	public function deleteShoppingCart($id)
	{
		$this->db->where('customer_id', $id);
		$result = $this->db->delete('shoppingcart');
		if ($result) {
			return true;
		} else return false;
	}


	public function GetShoppingCartByCustomerId($id){
		$data=$this->db->get_where('shoppingcart',array("customer_id" , $id))->result_array();

		if ($data) {
			return $data;
		}else return false;
	}

	public function GetShoppingCartPPackagesByCustomerId($id){
//		$data=$this->db->get_where('shoppingcart',array("customer_id" , $id))->result_array();
		$this->db->from('shopping_cart');
		$this->db->where("customer_id" , $id);
		$this->db->join('product_package', 'product_package.package_id =shopping_cart.product_package_id');
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}else return false;
	}

	public function getTotalPrice($id){
		$this->db->select_sum('price');
		$this->db->from('shoppingcart');
		$this->db->where('customer_id',$id);
		$data = $this->db->get()->result_array();
		if ($data) {
			return $data;
		}else return false;
	}
	public function GetPackagePrice($package_id){
		$this->db->select('total_price');
		$this->db->where('package_id',$package_id);
		$data = $this->db->get('product_package')->row();
		if ($data) {
			return $data->total_price;
		}else return false;
	}
	public function deletePPackageItem($id)
	{
		$this->db->delete('shopping_cart', array('shopping_id' => $id));
	}

	public function addPackageToCart($data=array())
	{
		$result=$this->db->insert('shopping_cart', $data);
		if ($result) {
			return true;
		}

	}



	public function getProductsFromShoppingCartByCustomerId($id)
	{
		$this->db->select('*');
		$this->db->from('shopping_cart');
		$this->db->where('customer_id',$id);
		return $this->db->get()->result_array();
	}
	public function addCount($itemID)
	{
		$this->db->set('count', 'count+1' , false);
		$this->db->where('shopping_cart_id', $itemID);
		$data = $this->db->update('shopping_cart');
		if ($data) {
			return true;
		}
	}
	public function decreseCount($itemID)
	{
		$this->db->set('count', 'count-1' , false);
		$this->db->where('shopping_cart_id', $itemID);
		$data = $this->db->update('shopping_cart');
		if ($data) {
			return true;
		}
	}
	public function UpdateFactorStatus($factorID)
	{
		$this->db->set('status', 1);
		$this->db->where('factor_id', $factorID);
		$data = $this->db->update('factor');
		if ($data) {
			return true;
		}
	}
	public function totalCountShoppingCart($customer_id)
	{
		$this->db->select_sum('count');
		$this->db->where('customer_id', $customer_id);
		$query = $this->db->get('shopping_cart')->row();
		if ($query) {
			return $query;
		}
	}
	public function deletePackageFromCart($itemID)
	{
		$this->db->delete('shopping_cart', array('shopping_cart_id' => $itemID));
	}
	public function getPackagesByFactorID($id)
	{
		$this->db->select('bought_package.* , product_package.teacher_id');
		$this->db->from('bought_package');
		$this->db->where('bought_package.factor_id',$id);
		$this->db->join('product_package' , 'product_package.package_id = bought_package.package_id');
		return $this->db->get()->result_array();
	}
}

