<?php include_once "header.php" ?>
  <!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- login page start -->
<section id="auth-login" class="row flexbox-container">
    <div class="col-xl-8 col-11">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- left section-login -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                        <div class="card-header pb-1">
                            <div class="card-title">
								<h1 class="text-center mb-2">کاربر گرامی </h1>
                                <h4 class="text-center mb-2">خوش آمدید</h4>
                            </div>
                        </div>
						<?php if(isset($_SESSION['error'])){ ?>
							<h4 style="color: red;font-size: 14px">
								<?php echo $_SESSION['error'] ?>
							</h4>
							<?php
							unset($_SESSION['error']);
						} ?>
                        <div class="card-content">
                            <div class="card-body">
<!--                                <div class="d-flex flex-md-row flex-column justify-content-around">-->
<!--                                    <a href="#" class="btn btn-social btn-google btn-block font-small-3 mr-md-1 mb-md-0 mb-1">-->
<!--                                        <i class="bx bxl-google font-medium-3"></i><span class="pl-50 d-block text-center">گوگل</span></a>-->
<!--                                    <a href="#" class="btn btn-social btn-block mt-0 btn-facebook font-small-3">-->
<!--                                        <i class="bx bxl-facebook-square font-medium-3"></i><span class="pl-50 d-block text-center">فیسبوک</span></a>-->
<!--                                </div>-->
<!--                                <div class="divider">-->
<!--                                    <div class="divider-text text-uppercase text-muted"><small>یا توسط ایمیل وارد شوید</small>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <form action="" method="post">
                                    <div class="form-group mb-50">
                                        <label class="text-bold-700" for="exampleInputEmail1"> نام کاربری </label>
										<input type="text" name="user-name" class="form-control text-left" id="exampleInputEmail1" dir="ltr">
									</div>
                                    <div class="form-group">
                                        <label class="text-bold-700" for="exampleInputPassword1">رمز عبور</label>
                                        <input type="password" name="user-password" class="form-control text-left" id="exampleInputPassword1" dir="ltr">
                                    </div>
<!--                                    <div class="form-group d-flex flex-md-row flex-column justify-content-between align-items-center">-->
<!--                                        <div class="text-left">-->
<!--                                            <div class="checkbox checkbox-sm">-->
<!--                                                <input type="checkbox" class="form-check-input" id="exampleCheck1">-->
<!--                                                <label class="checkboxsmall" for="exampleCheck1"><small>مرا به خاطر بسپار</small></label>-->
<!--                                            </div>-->
<!--                                        </div>-->
<!--                                        <div class="text-right line-height-2"><a href="auth-forgot-password.html" class="card-link"><small>رمز عبورتان را فراموش کرده اید؟</small></a></div>-->
<!--                                    </div>-->
                                    <button type="submit" name="login-submit" class="btn btn-primary glow w-100 position-relative">ورود<i id="icon-arrow" class="bx bx-left-arrow-alt"></i></button>
                                </form>
                                <hr>
                                <div class="text-center"><small class="mr-25">حسابی ندارید؟</small><a href="<?= base_url('Auth/CustomerSubmit')?>"><small>ثبت نام</small></a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- right section image -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <div class="card-content">
                        <img class="img-fluid" src="<?= base_url('portals/')?>assets/images/5219.jpg" alt="branding logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- login page ends -->

        </div>
      </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/vendors.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Page Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/polyfill.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/charts/apexcharts.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/dragula.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.date.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.time.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/legacy.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/daterange/moment.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
	<!-- END: Page Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/datatables.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/datatables/datatable.js"></script>
	<!-- BEGIN: Theme JS-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/configs/vertical-menu-dark.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/core/app-menu.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/core/app.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/components.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/footer.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/customizer.js"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/dashboard-analytics.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/extensions/sweet-alerts.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/form-tooltip-valid.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/select/form-select2.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/forms/select/select2.full.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/select/form-select2.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/extensions/sweet-alerts.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/polyfill.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/charts/chart.min.js"></script>
	<!--<script src="--><?//= base_url('portals/')?><!--assets/js/scripts/charts/chart-chartjs.js"></script>-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/page-users.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/app-invoice.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <!-- END: Page JS-->

  </body>
  <!-- END: Body-->
</html>
