<?php include_once 'application/views/dashboard/header.php'; ?>
<?php include_once 'application/views/dashboard/user-sidebar.php'; ?>
<?php include_once 'application/helpers/jalaliconvertor.php'; ?>
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1">فاکتور های شما</h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Zero configuration table -->

		<section id="column-selectors">
			<div class="row">
				<div class="col-12">
					<div class="card">

						<div class="card-content">
							<div class="card-body card-dashboard">

								<div class="table-responsive">
									<table class="table table-striped dataex-html5-selectors">
										<thead>
										<tr>
											<th>تاریخ فاکتور</th>
											<th>شماره فاکتور </th>
											<th>نشانی </th>
											<th>مبلغ کل</th>
											<th>مشاهده فاکتور</th>


										</tr>
										</thead>
										<tbody>
										<?php foreach ($factors as $d): ?>
											<tr>

												<td> <?php echo splitDate2($d['created_at']) ?></td>
												<td> <?php echo $d['factor_id']?></td>
												<td> <?php echo $d['address']?></td>
												<td> <?php echo $d['total_price'] ?></td>
												<td><a href="<?php echo base_url('Dashboard/User/showFactorDetail/') ?><?php echo $d['factor_id'] ?>">
														<button style="font-size: 14px;" class="btn btn-success"><i
																class="fas fa-trash" style="font-size: 17px"></i>
															مشاهده
														</button>
													</a>
												</td>



											</tr>
										<?php endforeach; ?>
										</tbody>
										<tfoot>
										<tr>
											<th>تاریخ فاکتور</th>
											<th>شماره فاکتور </th>
											<th>نشانی </th>
											<th>مبلغ کل</th>
											<th>مشاهده فاکتور</th>

										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Column selectors with Export Options and print table -->

		<!-- Scroll - horizontal and vertical table -->
		<!--/ Scroll - horizontal and vertical table -->
	</div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<?php include_once 'application/views/dashboard/footer.php' ?>
