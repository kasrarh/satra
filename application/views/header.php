<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title> کافه آنی</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?= base_url('portals/') ?>assets/images/ico/favicon.ico">
	<meta name="theme-color" content="#5A8DEE">

	<!-- BEGIN: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/assets/') ?>vendors/css/vendors.min.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/forms/select/select2.min.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/assets/') ?>vendors/css/tables/datatable/datatables.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/vendors/css/charts/apexcharts.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/forms/select/select2.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/vendors/css/charts/apexcharts.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/extensions/dragula.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/vendors/css/vendors.min.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/pickers/pickadate/pickadate.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/pickers/daterange/daterangepicker.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/pickers/datepicker-jalali/bootstrap-datepicker.min.css">
	<!-- END: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/vendors/css/animate/animate.css">
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/vendors/css/extensions/sweetalert2.min.css">
	<!-- BEGIN: Theme CSS-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/bootstrap-extended.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/colors.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/components.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/themes/dark-layout.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/themes/semi-dark-layout.css">
	<!-- END: Theme CSS-->

	<!-- BEGIN: Page CSS-->
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/css/core/menu/menu-types/vertical-menu.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/pages/dashboard-analytics.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url('portals/') ?>assets/css/pages/page-users.css">
<!--	<link rel="stylesheet" type="text/css" href="--><?//= base_url('portals/') ?><!--assets/css/pages/app-invoice.css">-->
	<link rel="stylesheet" type="text/css"
		  href="<?= base_url('portals/') ?>assets/css/plugins/forms/validation/form-validation.css">
	<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
	<!--	<script src="--><?php //echo base_url() ?><!--assets/tinymce/js/tinymce/tinymce.min.js"></script>-->
	<!---->
	<!---->
	<!--	<script>tinymce.init({selector: '.postBody', directionality: 'rtl'});</script>-->

	<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
	<style>
		input {
			font-family: Arial !important;
		}

	</style>
	<!-- END: Page CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

