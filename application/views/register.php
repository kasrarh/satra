<?php include_once "header.php" ?>

<!-- END: Head-->

  <!-- BEGIN: Body-->
  <body class="vertical-layout vertical-menu-modern dark-layout 1-column  navbar-sticky footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="dark-layout">
    <!-- BEGIN: Content-->
    <div class="app-content content">
      <div class="content-overlay"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><!-- register section starts -->
<section class="row flexbox-container">
    <div class="col-xl-8 col-10">
        <div class="card bg-authentication mb-0">
            <div class="row m-0">
                <!-- register section left -->
                <div class="col-md-6 col-12 px-0">
                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                        <div class="card-header pb-1">
                            <div class="card-title">
                                <h4 class="text-center mb-2">ثبت نام</h4>
                            </div>
                        </div>
                        <div class="text-center">
                            <p> <small class="line-height-2 d-inline-block"> لطفا جزئیات خود را برای ثبت نام وارد کرده و عضوی از کافه قنادی آنی شوید.</small>
                            </p>
                        </div>
						<?php if(isset($_SESSION['error'])){ ?>
						<h4 style="color: red;font-size: 14px">
							<?php echo $_SESSION['error'] ?>
						</h4>
						<?php
						unset($_SESSION['error']);
						} ?>
                        <div class="card-content">
                            <div class="card-body">
                                <form action="" method="post">
                                    <div class="form-group mb-50">

                                            <label for="inputfirstname4">نام و نام خانوادگی</label>
                                            <input type="text" name="fullname" class="form-control" required id="inputfirstname4">
                                    </div>
									<div class="form-group mb-50">
                                            <label for="inputfirstname4">شماره تماس</label>
                                            <input type="tel" name="phone" min="11" max="12" class="form-control" required id="inputfirstname4">
                                    </div>
                                    <div class="form-group mb-50">
                                        <label class="text-bold-700" for="exampleInputUsername1">نام کاربری</label>
										<input type="text" name="username" class="form-control text-left" id="loginUsername" minlength="3" required dir="ltr">
										<h2 class="font-weight-semibold cc_cursor" style="font-size: 12px" id="Usernamedenied"></h2>

									</div>
                                    <div class="form-group mb-50">
                                        <label class="text-bold-700" for="exampleInputEmail1">آدرس ایمیل</label>
										<input type="email" name="email" class="form-control text-left" id="loginEmail" required dir="ltr">
										<h2 class="font-weight-semibold cc_cursor" style="font-size: 12px" id="Emaildenied"></h2>
									</div>
                                    <div class="form-group mb-2">
                                        <label class="text-bold-700" for="exampleInputPassword1">رمز عبور</label>
                                        <input type="password" name="password" class="form-control text-left" id="exampleInputPassword1" required minlength="8" placeholder=" حداقل ۸ کارکتر" dir="ltr">
										<div class="text-left">
											<div class="checkbox checkbox-sm">
												<input type="checkbox" class="form-check-input"
													   id="exampleCheck1">
												<label class="checkboxsmall" for="exampleCheck1" onclick="myFunction()"><small>مشاهده </small></label>
											</div>
										</div>
                                    </div>
									<div class="form-group mb-2">
										<label class="text-bold-700" for="exampleInputPassword2">تکرار رمز عبور</label>
										<input type="password" name="passwordConfirm" class="form-control text-left" id="exampleInputPassword2" required minlength="8" placeholder="" dir="ltr">
									<h4 id="message" style="font-size: 14px;display: none"></h4>
									</div>
                                    <button type="submit" id="dynamicButton" name="submit" class="btn btn-primary glow position-relative w-100">ثبت نام<i id="icon-arrow" class="bx bx-left-arrow-alt"></i></button>
                                </form>
                                <hr>
                                <div class="text-center"><small class="mr-25">حساب کاربری دارید؟</small><a href="<?= base_url('Auth/loginCustomer')?>"><small>ورود</small> </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- image section right -->
                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                    <img class="img-fluid" src="<?= base_url('portals/')?>assets/images/5219.jpg" alt="branding logo">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- register section endss -->
        </div>
      </div>
    </div>
    <!-- END: Content-->


    <!-- BEGIN: Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/vendors.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
	<script src="<?= base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
	<!-- BEGIN Vendor JS-->

	<!-- BEGIN: Page Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/polyfill.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/charts/apexcharts.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/dragula.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.date.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/picker.time.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/pickadate/legacy.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/daterange/moment.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
	<!-- END: Page Vendor JS-->
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/datatables.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/datatables/datatable.js"></script>
	<!-- BEGIN: Theme JS-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/configs/vertical-menu-dark.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/core/app-menu.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/core/app.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/components.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/footer.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/customizer.js"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/dashboard-analytics.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/extensions/sweet-alerts.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/form-tooltip-valid.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/select/form-select2.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/forms/select/select2.full.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/forms/select/form-select2.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/extensions/sweet-alerts.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/sweetalert2.all.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/extensions/polyfill.min.js"></script>
	<script src="<?= base_url('portals/')?>assets/vendors/js/charts/chart.min.js"></script>
	<!--<script src="--><?//= base_url('portals/')?><!--assets/js/scripts/charts/chart-chartjs.js"></script>-->
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/page-users.js"></script>
	<script src="<?= base_url('portals/')?>assets/js/scripts/pages/app-invoice.js"></script>
	<!-- END: Theme JS-->

	<!-- BEGIN: Page JS-->
	<!-- END: Page JS-->

  </body>
<script type="text/javascript">

    $(document).ready(function () {
        $("#loginUsername").on("keyup", function () {
            var username =  $("#loginUsername").val();
            $.ajax({
                url: '<?= base_url('Auth/findUsername') ?>',
                method: 'GET',
                data: {username: username,table: 'customers'},
                dataType: 'json',
                success: function (response) {
                    if(response){
                        $("#Usernamedenied").text('نام کاربری موجود است.');
                        $("#Usernamedenied").css("color", "#027e16");
                        $("#dynamicButton").prop('disabled', false);
                    }else{
                        $("#Usernamedenied").text('نام کاربری قبلاً ایجاد شده است.');
                        $("#Usernamedenied").css("color", "#f93d4f");
                        $("#dynamicButton").prop('disabled', true);

                    }
                    console.log(response);
                }
            })

        })
    })
</script>
<script type="text/javascript">

    $(document).ready(function () {
        $("#loginEmail").on("keyup", function () {
            var email =  $("#loginEmail").val();
            $.ajax({
                url: '<?= base_url('Auth/findEmail') ?>',
                method: 'GET',
                data: {email: email},
                dataType: 'json',
                success: function (response) {
                    if(response){
                        $("#Emaildenied").text('');
                        $("#Emaildenied").css("color", "#027e16");
                    }else{
                        $("#Emaildenied").text('اکانت با این ایمیل قبلاً ایجاد شده است.');
                        $("#Emaildenied").css("color", "#f93d4f");
                    }
                    console.log(response);
                }
            })

        })
    })
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery-3.3.1.min.js"></script>
<script>
	function myFunction() {
		var x = document.getElementById("exampleInputPassword1");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
	$('#exampleInputPassword1, #exampleInputPassword2').on('keyup', function () {
		if ($('#exampleInputPassword1').val() === $('#exampleInputPassword2').val()) {
			$('#message').css('display', 'none');
		} else
			$('#message').html('رمز‌های عبور مشابه نیستد!').css('display', 'block');
	});
</script>
<!--<script type="text/javascript">-->
<!---->
<!--    $(document).ready(function () {-->
<!--        $("#loginEmail").on("keyup", function () {-->
<!--            var emailColor =  $("#Emaildenied").css("color");-->
<!--            var usernameColor =  $("#Usernamedenied").css("color");-->
<!--            if(usernameColor.css("color") && emailColor.css("color") == ){-->
<!--                usernameColor.css("color", "#027e16");-->
<!--                emailColor.css("color", "#027e16");-->
<!--            }else{-->
<!--                $("#Emaildenied").text('اکانت با این ایمیل قبلاً ایجاد شده است.');-->
<!--                $("#Emaildenied").css("color", "#f93d4f");-->
<!--            }-->
<!--        })-->
<!--    })-->
<!--</script>-->
<!-- END: Body-->
</html>
