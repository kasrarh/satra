<body class="vertical-layout vertical-menu-modern dark-layout 2-columns  navbar-sticky footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="dark-layout">

<!-- BEGIN: Header-->
<div class="header-navbar-shadow"></div>
<nav class="header-navbar main-header-navbar navbar-expand-lg navbar navbar-with-menu fixed-top navbar-dark">
	<div class="navbar-wrapper">
		<div class="navbar-container content">
			<div class="navbar-collapse" id="navbar-mobile">
				<div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
					<ul class="nav navbar-nav">
						<li class="nav-item mobile-menu d-xl-none mr-auto"><a
								class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i
									class="ficon bx bx-menu"></i></a></li>
					</ul>
					<!--					<ul class="nav navbar-nav bookmark-icons">-->
					<!--						<li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-email.html"-->
					<!--																  data-toggle="tooltip" data-placement="bottom"-->
					<!--																  title="ایمیل"><i class="ficon bx bx-envelope"></i></a>-->
					<!--						</li>-->
					<!--						<li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-chat.html"-->
					<!--																  data-toggle="tooltip" data-placement="bottom"-->
					<!--																  title="گفتگو"><i class="ficon bx bx-chat"></i></a>-->
					<!--						</li>-->
					<!--						<li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-todo.html"-->
					<!--																  data-toggle="tooltip" data-placement="bottom"-->
					<!--																  title="وظایف"><i class="ficon bx bx-check-circle"></i></a>-->
					<!--						</li>-->
					<!--						<li class="nav-item d-none d-lg-block"><a class="nav-link" href="app-calendar.html"-->
					<!--																  data-toggle="tooltip" data-placement="bottom"-->
					<!--																  title="تقویم"><i class="ficon bx bx-calendar-alt"></i></a>-->
					<!--						</li>-->
					<!--					</ul>-->
					<ul class="nav navbar-nav">
						<li class="nav-item d-none d-lg-block">
							<div class="bookmark-input search-input">
								<div class="bookmark-input-icon"><i class="bx bx-search primary"></i></div>
								<input class="form-control input" type="text" placeholder="جستجو ..." tabindex="0"
									   data-search="template-search">
								<ul class="search-list"></ul>
							</div>
						</li>
					</ul>
				</div>
				<ul class="nav navbar-nav float-right">
					<li class="dropdown dropdown-language nav-item"><a class="dropdown-toggle nav-link"
																	   id="dropdown-flag" href="#"
																	   data-toggle="dropdown" aria-haspopup="true"
																	   aria-expanded="false"><i
								class="flag-icon flag-icon-ir"></i><span class="selected-language">فارسی</span></a>
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag"><a
								class="dropdown-item" href="#" data-language="fa"><i
									class="flag-icon flag-icon-ir mr-50"></i> فارسی</a></div>
					</li>
					<li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i
								class="ficon bx bx-fullscreen"></i></a></li>
					<div class="search-input">
						<div class="search-input-icon"><i class="bx bx-search primary"></i></div>
						<input class="input" type="text" placeholder="جستجو ..." tabindex="-1"
							   data-search="template-search">
						<div class="search-input-close"><i class="bx bx-x"></i></div>
						<ul class="search-list"></ul>
					</div>
					</li>
					<ul class="dropdown-menu dropdown-menu-media">
						<li class="dropdown-menu-header">
							<div class="dropdown-header px-1 py-75 d-flex justify-content-between"><span
									class="notification-title">7 اعلان جدید</span><span
									class="text-bold-400 cursor-pointer">علامت خوانده شده به همه</span></div>
						</li>
						<li class="scrollable-container media-list"><a class="d-flex justify-content-between"
																	   href="javascript:void(0)">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar mr-1 m-0"><img
												src="<?= base_url('portals/assets/')?>images/portrait/small/avatar-s-11.jpg"
												alt="avatar" height="39" width="39"></div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">تبریک بابت دریافت جوایز</span>
											در مسابقات سالانه</h6><small class="notification-text">15 اردیبهشت 12:32
											ب.ظ</small>
									</div>
								</div>
							</a>
							<div class="d-flex justify-content-between read-notification cursor-pointer">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar mr-1 m-0"><img
												src="<?= base_url('portals/assets/')?>images/portrait/small/avatar-s-16.jpg"
												alt="avatar" height="39" width="39"></div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">پیام جدید</span>
											دریافت شد</h6><small class="notification-text">شما 18 پیام خوانده نشده
											دارید</small>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between cursor-pointer">
								<div class="media d-flex align-items-center py-0">
									<div class="media-left pr-0"><img class="mr-1"
																	  src="<?= base_url('portals/assets/')?>images/icon/sketch-mac-icon.png"
																	  alt="avatar" height="39" width="39"></div>
									<div class="media-body">
										<h6 class="media-heading"><span
												class="text-bold-500">به روز رسانی آماده است</span></h6><small
											class="notification-text">لورم ایپسوم متن ساختگی با تولید
											سادگی</small>
									</div>
									<div class="media-right pl-0">
										<div class="row border-left text-center">
											<div class="col-12 px-50 py-50 border-bottom">
												<h6 class="media-heading text-bold-500 mb-0">به‌روزرسانی</h6>
											</div>
											<div class="col-12 px-50 py-50">
												<h6 class="media-heading mb-0">بستن</h6>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between cursor-pointer">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar bg-primary bg-lighten-5 mr-1 m-0 p-25"><span
												class="avatar-content text-primary font-medium-2">ل‌د</span>
										</div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">مشتری جدید</span> ثبت
											نام کرد</h6><small class="notification-text">1 ساعت پیش</small>
									</div>
								</div>
							</div>
							<div class="cursor-pointer">
								<div class="media d-flex align-items-center justify-content-between">
									<div class="media-left pr-0">
										<div class="media-body">
											<h6 class="media-heading">پیشنهاد های جدید</h6>
										</div>
									</div>
									<div class="media-right">
										<div class="custom-control custom-switch">
											<input class="custom-control-input" type="checkbox" checked
												   id="notificationSwtich">
											<label class="custom-control-label" for="notificationSwtich"></label>
										</div>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between cursor-pointer">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar bg-danger bg-lighten-5 mr-1 m-0 p-25"><span
												class="avatar-content"><i class="bx bxs-heart text-danger"></i></span>
										</div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">نرم افزار</span> تایید
											شد</h6><small class="notification-text">6 ساعت پیش</small>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between read-notification cursor-pointer">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar mr-1 m-0"><img
												src="<?= base_url('portals/assets/')?>images/portrait/small/avatar-s-4.jpg"
												alt="avatar" height="39" width="39"></div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">فایل جدید</span> ارسال
											شد</h6><small class="notification-text">4 ساعت پیش</small>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between cursor-pointer">
								<div class="media d-flex align-items-center">
									<div class="media-left pr-0">
										<div class="avatar bg-rgba-danger m-0 mr-1 p-25">
											<div class="avatar-content"><i class="bx bx-detail text-danger"></i>
											</div>
										</div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">گزارش بودجه</span>
											ایجاد شد</h6><small class="notification-text">25 ساعت پیش</small>
									</div>
								</div>
							</div>
							<div class="d-flex justify-content-between cursor-pointer">
								<div class="media d-flex align-items-center border-0">
									<div class="media-left pr-0">
										<div class="avatar mr-1 m-0"><img
												src="<?= base_url('portals/assets/')?>images/portrait/small/avatar-s-16.jpg"
												alt="avatar" height="39" width="39"></div>
									</div>
									<div class="media-body">
										<h6 class="media-heading"><span class="text-bold-500">مشتری جدید</span>
											دیدگاهی ارسال کرد</h6><small class="notification-text">2 روز پیش</small>
									</div>
								</div>
							</div>
						</li>
						<li class="dropdown-menu-footer"><a
								class="dropdown-item p-50 text-primary justify-content-center"
								href="javascript:void(0)">خواندن همه اعلان ها</a></li>
					</ul>
					</li>
					<li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link"
																   href="#" data-toggle="dropdown">

							<div class="user-nav d-sm-flex d-none"><span
																		class="user-name" style="margin-top:-18px"> 
								
								<span>خروج</span></a>
						<div class="dropdown-menu pb-0">

							<!--							<div class="dropdown-divider mb-0"></div>-->
							<a class="dropdown-item" href="<?= base_url('Auth/Logout')?>"><i class="bx bx-power-off mr-50"></i>
								خروج</a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<!-- END: Header-->


<!-- BEGIN: Main Menu-->
<div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
	<div class="navbar-header">
		<ul class="nav navbar-nav flex-row">
			<li class="nav-item mr-auto"><a class="navbar-brand" href="">

					<h2 class="brand-text mb-0">AniCake</h2></a></li>
			<li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block primary" data-ticon="bx-disc"></i></a></li>
		</ul>
	</div>
	<div class="shadow-bottom"></div>
	<div class="main-menu-content">
		<ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="lines">
			<!--			<li class=" nav-item"><a href="../../html/vertical-menu-template-dark/index.html"><i class="menu-livicon" data-icon="desktop"></i><span class="menu-title" data-i18n="Dashboard">داشبورد</span><span class="badge bg-rgba-danger text-danger badge-pill badge-round float-right mr-2">2</span></a>-->
			<!--				<ul class="menu-content">-->
			<!--					<li><a href="dashboard-ecommerce.html"><i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="eCommerce">تجارت الکترونیک</span></a>-->
			<!--					</li>-->
			<!--					<li class="active"><a href="dashboard-analytics.html"><i class="bx bx-left-arrow-alt"></i><span class="menu-item" data-i18n="Analytics">آمار تحلیلی</span></a>-->
			<!--					</li>-->
			<!--				</ul>-->
			<!--			</li>-->
			<li class=" navigation-header"><span>پرتال مدیریت </span>
			</li>





			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showCategory')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    دسته بندی </span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showItem')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    آیتم ها </span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showCustomer')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشتریان</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showUser')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشترکین</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showTodayCustomerFactor')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشاهده فاکتور امروز مشتریان</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showTodayUserFactor')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشاهده فاکتور امروز کاربران</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showUserFactor')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشاهده فاکتور کاربران</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showCustomerFactor')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشاهده فاکتور مشتریان</span></a>
			<li class=" nav-item"><a href="<?=base_url('Dashboard/Show/showUserSoldCakes')?>"><i class="" data-icon=""></i><span class="menu-title" data-i18n="Invoice">    مشاهده آمار فروش</span></a>



		</ul>
	</div>
</div>
