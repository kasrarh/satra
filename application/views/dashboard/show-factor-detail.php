<?php include_once 'header.php'; ?>
<?php include_once 'sidebar.php'; ?>
<?php include_once 'application/helpers/jalaliconvertor.php'; ?>
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1">فاکتور مشتریان</h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Zero configuration table -->

		<section id="column-selectors">
			<div class="row">
				<div class="col-12">
					<div class="card">


						<div class="card-content">
							<div class="card-body card-dashboard">

								<div class="table-responsive">
									<table class="table table-striped dataex-html5-selectors">
										<thead>
										<tr>
											<th>محصول</th>
											<th>تعداد</th>
											<th>قیمت</th>
										</tr>
										</thead>
										<tbody>
<!--										--><?php //foreach ($data as $d): ?>
										<?php foreach ($res as $r): ?>
											<tr>

												<td> <?php echo $r['name']?></td>
												<td> <?php echo $r['amount']?></td>
													<td> <?php echo $r['price']?></td>




											</tr>
										<?php endforeach; ?>
<!--										--><?php //endforeach; ?>
										</tbody>
										<tfoot>
										<tr>
											<th>محصول</th>
											<th>تعداد</th>
											<th>قیمت</th>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Column selectors with Export Options and print table -->

		<!-- Scroll - horizontal and vertical table -->
		<!--/ Scroll - horizontal and vertical table -->
	</div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<?php include_once 'footer.php' ?>
