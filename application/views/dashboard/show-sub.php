<?php include_once 'header.php'; ?>
<?php include_once 'sidebar.php'; ?>
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1" style="color: white !important;">زیر دسته بندی <?= $data1[0]['name'] ?></h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Zero configuration table -->

		<section id="column-selectors">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<a href="<?=base_url('Dashboard/Show/addSubCategory/')?>">
								<button class="btn btn-success">افزودن زیر دسته بندی </button>
							</a>
						</div>

						<div class="card-content">
							<div class="card-body card-dashboard">

								<div class="table-responsive">
									<table class="table table-striped dataex-html5-selectors">
										<thead>
										<tr>
											<th>عنوان زیر دسته بندی </th>
											<th>وکتور</th>
											<th>ویرایش</th>
											<th>  حذف</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach ($data2 as $d): ?>
											<tr>

												<td> <?php echo $d['name'].'/'.$d['en_name']?></td>
												<td><img src="<?php echo $d['image_url'] ?>" width="80px" height="80px" alt=""></td>
												<td>
													<a href="<?php echo base_url('Dashboard/Show/editSubCategory/') ?><?php echo $d['category2_id'] ?>">
														<button style="font-size: 14px;" class="btn btn-warning"><i
																	class="fas fa-warning" style="font-size: 17px"></i>
															ویرایش
														</button>
													</a>
												</td>
												<td><a href="<?php echo base_url('Dashboard/Show/deleteSubCategory/') ?><?php echo $d['category2_id'] ?>">
														<button style="font-size: 14px;" class="btn btn-danger"><i
																class="fas fa-trash" style="font-size: 17px"></i>
															حذف
														</button>
													</a></td>


											</tr>
										<?php endforeach; ?>
										</tbody>
										<tfoot>
										<tr>
											<th>عنوان زیر دسته بندی </th>
											<th>وکتور</th>
											<th>ویرایش</th>
											<th>  حذف</th>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Column selectors with Export Options and print table -->

		<!-- Scroll - horizontal and vertical table -->
		<!--/ Scroll - horizontal and vertical table -->
	</div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<?php include_once 'footer.php' ?>
