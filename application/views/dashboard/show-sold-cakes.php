<?php include_once 'header.php'; ?>
<?php include_once 'sidebar.php'; ?>
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
            <div class="content-header-left col-12 mb-2 mt-1">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h5 class="content-header-title float-left pr-1">آمار فروش</h5>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb p-0 mb-0">
                                <li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Zero configuration table -->

        <section id="column-selectors">
            <form action="<?= base_url('Dashboard/Show/getUserSoldViaFilter') ?>" method="post"
                enctype="multipart/form-data">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <section id="pick-a-time">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row match-height">

                                            <div class="col-md-3 mb-1">
                                                <h6>ساعت شروع</h6>
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input name="start_t" type="text"
                                                        class="form-control pickatime-format" placeholder="انتخاب زمان"
                                                        autocomplete="off">
                                                    <div class="form-control-position">
                                                        <i class="bx bx-history"></i>
                                                    </div>
                                                </fieldset>
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <h6> تاریخ شروع</h6>
                                                <fieldset class="form-group position-relative has-icon-left">

                                                    <input autocomplete="off" name="start_d" type="text"
                                                        class="form-control shamsi-datepicker-list"
                                                        placeholder="انتخاب تاریخ">

                                                </fieldset>
                                            </div>
                                            <div class="col-md-3 mb-1">
                                                <h6>ساعت پایان</h6>
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <input name="end_t" type="text"
                                                        class="form-control pickatime-format" placeholder="انتخاب زمان"
                                                        autocomplete="off">
                                                    <div class="form-control-position">
                                                        <i class="bx bx-history"></i>
                                                    </div>
                                                </fieldset>
                                            </div>

                                            <div class="col-md-3 mb-1">
                                                <h6> تاریخ پایان</h6>
                                                <fieldset class="form-group position-relative has-icon-left">

                                                    <input autocomplete="off" name=" end_d" type="text"
                                                        class="form-control shamsi-datepicker-list"
                                                        placeholder="انتخاب تاریخ">

                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card-header form-group">
                                        <a href="<?=base_url('Dashboard/Show/addItem/')?>">
                                            <button type="submit" name="submit" class="btn btn-success">اعمال فیلتر
                                            </button>
                                        </a>
                                    </div>
                            </section>

                            <div class="col-12">


                                <div class="card-content">
                                    <div class="card-body card-dashboard">

                                        <div class="table-responsive">
                                            <table class="table table-striped dataex-html5-selectors" id="myTable">
                                                <thead>
                                                    <tr>
                                                        <th>تاریخ</th>
                                                        <th> نام کافه </th>
                                                        <?php foreach($items as $i){ ?>
                                                        <th> <?= $i['name'] ?> </th>
                                                        <?php } ?>
                                                        <th> جمع سفارشات</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                    <?php  foreach ($users as $u){  $totalAmount=0; $userNumber=1;?>
                                                    <tr>
                                                        <?php foreach ($sales as $s) ?>
                                                        <td> <?php echo splitDate2($s['created_at'])?> </td>
                                                        <td> <?php echo $u['store_name'] ?></td>
                                                        <?php foreach($items as $i){ 
                                                    $amount=0;
                                                    $itemNumber=1;
                                                
                                                  foreach($sales as $s): 
                                                  if($s['user_id']==$u['user_id'] && $i['id']==$s['p_id']){
                                                      $amount=$amount+$s['f_amount'];

                                                  } ?>


                                                        <?php  endforeach;  ?>
                                                        <td id="<?= $userNumber.'.'.$itemNumber ?>">
                                                            <?php $itemNumber++ ; echo $amount ?></td>

                                                        <?php $totalAmount=$totalAmount+$amount;  }  ?>

                                                        <td> <?php $userNumber++; echo $totalAmount; ?></td>


                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <th>-</th>
                                                        <th> جمع کل سفارشات </th>
                                                        <?php $total=0; foreach($totalProductAmount as $t){ ?>
                                                        <th> <?= $t ?> </th>
                                                        <?php $total=$total+$t; } ?>
                                                        <th> <?= $total; ?> </th>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                         <th>تاریخ</th>
                                                        <th> نام کافه </th>
                                                        <?php foreach($items as $i){ ?>
                                                        <th> <?= $i['name'] ?> </th>
                                                        <?php } ?>
                                                        <th> جمع سفارشات</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </form>
        </section>
        <!-- Column selectors with Export Options and print table -->

        <!-- Scroll - horizontal and vertical table -->
        <!--/ Scroll - horizontal and vertical table -->
    </div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>

<?php include_once 'footer.php' ?>