<!DOCTYPE html>
<html class="loading" lang="fa" data-textdirection="rtl" dir="rtl">
<!-- BEGIN: Head-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<title><?= $Status ?></title>
	<link rel="shortcut icon" type="image/x-icon" href="<?=base_url('portals/')?>assets/images/ico/favicon.ico">
	<meta name="theme-color" content="#5A8DEE">

	<!-- BEGIN: Vendor CSS-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/vendors/css/vendors.min.css">
	<!-- END: Vendor CSS-->

	<!-- BEGIN: Theme CSS-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/bootstrap-extended.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/colors.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/components.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/themes/dark-layout.css">
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/themes/semi-dark-layout.css">
	<!-- END: Theme CSS-->

	<!-- BEGIN: Page CSS-->
	<link rel="stylesheet" type="text/css" href="<?=base_url('portals/')?>assets/css/core/menu/menu-types/vertical-menu.css">
	<!-- END: Page CSS-->

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern dark-layout 1-column  navbar-sticky footer-static  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column" data-layout="dark-layout" style="background-color: <?= $backgroundColor ?>;color: white!important;">
<!-- BEGIN: Content-->
<div class="app-content content" >
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
		</div>
		<div class="content-body"><!-- maintenance start -->
			<section class="row flexbox-container">
				<div class="col-xl-7 col-md-8 col-12">
					<div class="card bg-transparent shadow-none">
						<div class="card-content">
							<div class="card-body text-center bg-transparent miscellaneous">
								<h1 class="error-title my-1" style="color: whitesmoke!important;"><?= $Status ?></h1>
								<?php if($tracking_code){ ?>
								<p class="px-2" STYLE="color: whitesmoke!important;">
									کد پیگیری درگاه پرداخت: <?= $tracking_code ?>
								</p>
								<?php } ?>
								<?php if($_SESSION['isUser']==true){ ?>
									<a href="<?= base_url('Dashboard/User/showUserFactor') ?>" class="btn btn-primary round glow mt-2">مشاهده فاکتور</a>
								<?php }elseif($_SESSION['isCustomer']==true){ ?>
									<a href="<?= base_url('Dashboard/Customer/showCustomerFactor') ?>" class="btn btn-primary round glow mt-2">مشاهده فاکتور</a>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- maintenance end -->
		</div>
	</div>
</div>
<!-- END: Content-->


<!-- BEGIN: Vendor JS-->
<script src="<?=base_url('portals/')?>assets/vendors/js/vendors.min.js"></script>
<script src="<?=base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
<script src="<?=base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="<?=base_url('portals/')?>assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?=base_url('portals/')?>assets/js/scripts/configs/vertical-menu-dark.js"></script>
<script src="<?=base_url('portals/')?>assets/js/core/app-menu.js"></script>
<script src="<?=base_url('portals/')?>assets/js/core/app.js"></script>
<script src="<?=base_url('portals/')?>assets/js/scripts/components.js"></script>
<script src="<?=base_url('portals/')?>assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<!-- END: Page JS-->
<?php if($tracking_code){ ?>
	<script>
		$(document).ready(function() {
			localStorage.clear();
		});
	</script>
<?php } ?>
</body>
<!-- END: Body-->
</html>
