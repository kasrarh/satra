<?php include_once 'header.php'; ?>
<?php include_once 'sidebar.php'; ?>
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1">ایتم ها</h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Zero configuration table -->

		<section id="column-selectors">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="card-header">
							<a href="<?=base_url('Dashboard/Show/addItem/')?>">
								<button class="btn btn-success">افزودن ایتم </button>
							</a>
						</div>

						<div class="card-content">
							<div class="card-body card-dashboard">

								<div class="table-responsive">
									<table class="table table-striped dataex-html5-selectors">
										<thead>
										<tr>
											<th>عنوان ایتم </th>
											<th> دسته بندی </th>
											<th>توضیحات</th>
											<th>قیمت مشتری</th>
											<th>قیمت کافه</th>
											<th>عکس</th>
											<th>  ویرایش</th>
											<th>  حذف</th>
										</tr>
										</thead>
										<tbody>
										<?php foreach ($data as $d): ?>
											<tr>

												<td> <?php echo $d['i_name'] ?></td>
												<td> <?php echo $d['c_name']?></td>
												<td> <?php echo $d['description'] ?></td>
												<td> <?php echo $d['price_normal'] ?></td>
												<td> <?php echo $d['price_special'] ?></td>
												<td><img src="<?php echo $d['i_img'] ?>" width="80px" height="80px" alt=""></td>
												<td><a href="<?php echo base_url('Dashboard/Show/editItem/') ?><?php echo $d['id'] ?>">
														<button style="font-size: 14px;" class="btn btn-warning"><i
																class="fas fa-trash" style="font-size: 17px"></i>
															ویرایش
														</button>
													</a></td>
												<td><a href="<?php echo base_url('Dashboard/Show/deleteItem/') ?><?php echo $d['id'] ?>">
														<button style="font-size: 14px;" class="btn btn-danger"><i
																	class="fas fa-trash" style="font-size: 17px"></i>
															حذف
														</button>
													</a></td>


											</tr>
										<?php endforeach; ?>
										</tbody>
										<tfoot>
										<tr>
											<th>عنوان ایتم </th>
											<th> دسته بندی </th>
											<th>توضیحات</th>
											<th>قیمت مشتری</th>
											<th>قیمت کافه</th>
											<th>عکس</th>
											<th>  ویرایش</th>
											<th>  حذف</th>
										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Column selectors with Export Options and print table -->

		<!-- Scroll - horizontal and vertical table -->
		<!--/ Scroll - horizontal and vertical table -->
	</div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<?php include_once 'footer.php' ?>
