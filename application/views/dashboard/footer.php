<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-dark">

</footer>
<!-- END: Footer-->


<!-- BEGIN: Vendor JS-->
<script src="<?=base_url('portals/assets/')?>vendors/js/vendors.min.js"></script>
<script src="<?=base_url('portals/assets/')?>fonts/LivIconsEvo/js/LivIconsEvo.tools.min.js"></script>
<script src="<?=base_url('portals/assets/')?>fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="<?=base_url('portals/assets/')?>fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/polyfill.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/charts/apexcharts.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/dragula.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/pickadate/picker.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/pickadate/legacy.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/daterange/moment.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/pickers/datepicker-jalali/bootstrap-datepicker.fa.min.js"></script>
<!-- END: Page Vendor JS-->
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/datatables.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/buttons.bootstrap.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/datatables/datatable.js"></script>
<!-- BEGIN: Theme JS-->
<script src="<?=base_url('portals/assets/')?>js/scripts/configs/vertical-menu-dark.js"></script>
<script src="<?=base_url('portals/assets/')?>js/core/app-menu.js"></script>
<script src="<?=base_url('portals/assets/')?>js/core/app.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/components.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/footer.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/customizer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?=base_url('portals/assets/')?>js/scripts/pages/dashboard-analytics.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/extensions/sweet-alerts.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/forms/form-tooltip-valid.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/forms/select/form-select2.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/forms/select/select2.full.min.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/forms/select/form-select2.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/extensions/sweet-alerts.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/sweetalert2.all.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/polyfill.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/charts/chart.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/editors/quill/katex.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/editors/quill/highlight.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/editors/quill/quill.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/extensions/jquery.steps.min.js"></script>
<script src="<?=base_url('portals/assets/')?>vendors/js/forms/validation/jquery.validate.min.js"></script>
<!--<script src="--><?//= base_url('portals/')?><!--assets/js/scripts/charts/chart-chartjs.js"></script>-->
<script src="<?=base_url('portals/assets/')?>js/scripts/pages/page-users.js"></script>
<script src="<?=base_url('portals/assets/')?>js/scripts/pages/app-invoice.js"></script>
<!-- END: Page JS-->

</body>
<!-- END: Body-->
</html>
