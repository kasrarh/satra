<?php include_once 'header.php'; ?>
<?php include_once 'sidebar.php'; ?>
<?php include_once 'application/helpers/jalaliconvertor.php'; ?>
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1">فاکتور مشتریان</h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href=""><i class="bx bx-home-alt"></i></a>

							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Zero configuration table -->

		<section id="column-selectors">
			<div class="row">
				<div class="col-12">
					<div class="card">


						<div class="card-content">
							<div class="card-body card-dashboard">

								<div class="table-responsive">
									<table class="table table-striped dataex-html5-selectors">
										<thead>
										<tr>
											<th>تاریخ فاکتور</th>
											<th>شماره فاکتور </th>
											<th>نام مشتری</th>
											<th>شماره تماس</th>
											<th>مبلغ کل</th>
											<th>آدرس</th>
											<th>وضعیت پرداخت</th>
											<th>مشاهده فاکتور</th>


										</tr>
										</thead>
										<tbody>
										<?php foreach ($data as $d): ?>
											<tr>

												<td> <?php echo splitDate2($d['factor_date']) ?></td>
												<td> <?php echo $d['factor_id']?></td>
												<td> <?php echo $d['name'] ?></td>
												<td> <?php echo $d['phone'] ?></td>
												<td> <?php echo $d['total_price'] ?></td>
												<td> <?php echo $d['f_address'] ?></td>
												<?php if($d['status']==1){ ?>
												<td style="color:green"> موفق</td>
												<?php }elseif($d['status']==0){ ?>
												<td style="color:red">  ناموفق</td>
												<?php } ?>

												<td><a href="<?php echo base_url('Dashboard/Show/showFactorDetail/') ?><?php echo $d['factor_id'] ?>">
														<button style="font-size: 14px;" class="btn btn-success"><i
																class="fas fa-trash" style="font-size: 17px"></i>
															مشاهده
														</button>
													</a>
												</td>



											</tr>
										<?php endforeach; ?>
										</tbody>
										<tfoot>
										<tr>
											<th>تاریخ فاکتور</th>
											<th>شماره فاکتور </th>
											<th>نام مشتری</th>
											<th>شماره تماس</th>
											<th>مبلغ کل</th>
											<th>آدرس</th>
											<th>وضعیت پرداخت</th>
											<th>مشاهده فاکتور</th>

										</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- Column selectors with Export Options and print table -->

		<!-- Scroll - horizontal and vertical table -->
		<!--/ Scroll - horizontal and vertical table -->
	</div>
</div>
<!-- demo chat-->

<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<?php include_once 'footer.php' ?>
