<?php include_once "header.php" ?>
<?php include_once "sidebar.php" ?>

<!-- BEGIN: Content-->
<div class="app-content content">
	<div class="content-overlay"></div>
	<div class="content-wrapper">
		<div class="content-header row">
			<div class="content-header-left col-12 mb-2 mt-1">
				<div class="row breadcrumbs-top">
					<div class="col-12">
						<h5 class="content-header-title float-left pr-1">پورتال مدیریت</h5>
						<div class="breadcrumb-wrapper">
							<ol class="breadcrumb p-0 mb-0">
								<li class="breadcrumb-item"><a href="index.php"><i class="bx bx-home-alt"></i></a>
								</li>
								<li class="breadcrumb-item"><a href="#">ایتم ها</a>
								</li>
								<li class="breadcrumb-item active">اضافه کردن
								</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="content-body"><!-- Basic Inputs start -->
			<section id="basic-input">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">فرم افزودن زیر دسته بندی </h4>
							</div>
							<form class="new-added-form" style="padding: 15px" method="post" enctype="multipart/form-data">
								<div class="row">
									<div class="col-xl-3 col-lg-6 col-12 form-group">
										<label> نام </label>

										<input type="text" placeholder="" class="form-control" name="name" value="" required>
									</div>

									<div class="col-xl-4 col-lg-8 col-12 form-group">
										<label> توضیحات </label>

										<input type="text" placeholder="" class="form-control" name="description" value="" required>
									</div>

									<div class="col-xl-3 col-lg-6 col-12 form-group">
										<label> قیمت مشتری</label>

										<input type="text" placeholder="" class="form-control" name="price_normal" value="" required>
									</div>


									<div class="col-xl-3 col-lg-6 col-12 form-group">
										<label> قیمت کافه</label>

										<input type="text" placeholder="" class="form-control" name="price_special" value="" required>
									</div>

									<div class="col-xl-3 col-lg-6 col-12 form-group">
										<label>دستبندی </label>
										<select class="form-control" name="category_id">
											<?php foreach ($category as $d): ?>

												<option value="<?=$d['category_id']?>"><?=$d['name']?></option>
											<?php  endforeach; ?>
										</select>
									</div>


									<div class="col-xl-3 col-lg-6 col-12">
										<fieldset class="form-group">
											<label for="basicInputFile">عکس</label>
											<div class="custom-file">
												<input type="file" class="custom-file-input" name="files[]" id="inputGroupFile01">
												<label class="custom-file-label" for="inputGroupFile01">انتخاب فایل</label>
											</div>
										</fieldset>
									</div>


									<div class="col-12 form-group mg-t-8">
										<button type="submit" class="btn btn-success" name="submit">ذخیره</button>
									</div>
								</div>

							</form>

						</div>
					</div>
				</div>

			</section>

			<!-- Basic Inputs end -->

			<!-- Input Style start -->

			<!-- Input Style end -->

			<!-- Horizontal Input start -->

			<!-- Horizontal Input end -->

			<!-- Basic File Browser start -->


		</div>
	</div>
</div>
<!-- END: Content-->


<!-- Buynow Button-->
<div class="buy-now"><a href="#" target="_blank" class="btn btn-danger">ارتباط با ما</a>

</div>
<!-- demo chat-->
<div class="sidenav-overlay"></div>
<div class="drag-target"></div>
<script type="text/javascript">

	$(document).ready(function () {

		$("#category").on("change", function () {


			var category = $('#category').find("option:selected").val();
			console.log(category);

			$.ajax({
					url: '<?php echo base_url('Dashboard/Products/getTeachers/')?>' + category,
					method: 'GET',
					dataType: 'json',
					success: function (response) {
						console.log(response);
						var newArr = response.map(function (val) {
								var option = document.createElement("option");
								option.text = val['fullname'];
								option.value = val['id'];
								var select = document.getElementById("teacher");
								select.appendChild(option);

							}
						)
					}
				}
			)
		});
	})

</script>
<script>
	kamaDatepicker('date');
</script>
<?php include_once "footer.php" ?>
