<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Mobile Coffee</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('menu_assets/') ?>css/style.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url('menu_assets/') ?>css/reset-flexbox.css"/>
	<link rel="stylesheet" type="text/css" href="<?= base_url('menu_assets/') ?>css/aos.css"/>
	<script src="<?= base_url('menu_assets/') ?>aos.js"></script>

	<script src="<?php echo base_url('portals/'); ?>assets/js/jquery-3.3.1.min.js"></script>


	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style>


@media (max-width: 576px) {
	.nav_item{
		   
		  margin-top:-33%
	   }
   }
@media (min-width: 576px) {
	.nav_item{
		   
           margin-top:-33%
        }
   }


   @media (min-width: 768px) {
	.nav_item{
		   
        margin-top: -8%;
    margin-left: 177px;
        }
   }


   @media (min-width: 992px) {
    .nav_item{
		   
        margin-top: -10%;
    margin-left: 34%;
}
        }
   }


   @media (min-width: 1200px) {
    .nav_item{
		   
        margin-top: -12%;
    margin-left: 33%;
        }
   }


@media (max-width: 576px) {
	.login{
		   
		   position: relative;
		   top:40px;
			  right: 33px;
	   }
   }
@media (min-width: 576px) {
	.login{
		   
		   position: relative;
		   top:40px;
			  right: 33px;
	   }
   }


   @media (min-width: 768px) {
	   .login{
		   position: relative;
		   top: 15px;
			  right: -30px;
		   
	 }
   }


   @media (min-width: 992px) {
	   .login{
		   position: relative;
		   top: 15px;
			  right: 24px;
	 }
   }


   @media (min-width: 1200px) {
	   .login{
	  
		   position: relative;
		   top: 46px;
			  right: 70px;
	 }
   }

   @media (max-width: 576px) {
			.count{
		   
			    position: relative;
    			top: -32px;
   				right: 118px;
			}
		}	
   
@media (min-width: 576px) {
			.count{
		   
			    position: relative;
    			top: 15px;
   				right: 6px;
			}
		}


		@media (min-width: 768px) {
			.count{
				position: relative;
    			top: 15px;
   				right: -30px;
				
		  }
		}


		@media (min-width: 992px) {
			.count{
				position: relative;
    			top: 15px;
   				right: 24px;
		  }
		}


		@media (min-width: 1200px) {
			.count{
		   
				position: relative;
    			top: -42px;
   				right: 3px;
		  }
		}	

		@media (max-width: 576px) {
			.carpet {
		   
				margin: -15% auto 0;
    			width: 40%;
			}
		}
@media (min-width: 576px) {
			.carpet {
		   
				margin: -15% auto 0;
    			width: 40%;
			}
		}


		@media (min-width: 768px) {
			.carpet {
		   
				margin: -8% auto 0;
    			width: 15%;
		  }
		}


		@media (min-width: 992px) {
			.carpet {
                margin: -7% auto 0;
    width: 24%;
		  }
		}


		@media (min-width: 1200px) {
			.carpet {
		   
				margin: -4% auto 0;
   				 width: 16%;
		  }
		}		


		@media (max-width: 576px) {
			.signin {
		   
             position:relative;
             right:-286px;
            top: 13px;
			}
		}

@media (min-width: 576px) {
			.signin {
		   
                position: relative;
    right: -595px;
    top: 16px;
			}
		}


		@media (min-width: 768px) {
			.signin {
		   
                position: relative;
    right: -737px;
    top: 13px;
		  }
		}


		@media (min-width: 992px) {
			.signin {
		   
                position: relative;
    right: -888px;
    top: 20px;
		  }
		}


		@media (min-width: 1200px) {
			.signin {
		   
		   position:relative;
		   right:-1381px;
		  top: 18px;
		  }
		}



		@media (max-width: 576px) {
			.shopping {
		   
             position:relative;
            left:10px;
            top: 13px;
			}
		}

		@media (min-width: 576px) {
			.shopping {
		   
             position:relative;
            left:10px;
            top: 13px;
			}
		}


		@media (min-width: 768px) {
			.shopping {
		   
		   position:relative;
		left:10px;
		  top: 13px;
		  }
		}


		@media (min-width: 992px) {
			.shopping {
		   
		   position:relative;
		left:10px;
		  top: 13px;
		  }
		}

	



		@media (max-width: 576px) {
			.imgLogo {
		     width: 60%;
             position: relative;
             left: 80px;
            top: -166px;
			}
		}

	
			@media (min-width: 576px) {
			.imgLogo {
		       width: 62%;
    position: relative;
    left: 115px;
    top: -274px;
			}
		}


		@media (min-width: 768px) {
			.imgLogo {
                width: 168px;
    position: relative;
    left: 355px;
    top: -117px;
			}
		}

	
		@media (min-width: 992px) {
			.imgLogo {

                width: 304px;
    position: relative;
    left: 360px;
    top: -237px;

			}
		}


		@media (min-width: 1200px) {
			.imgLogo {
		     width: 368px;
             position: relative;
             left: 575px;
            top: -232px;
			}
		}
	
	

		@media (max-width: 576px) {
			.category {

				margin-top: -115px;
				margin-left: 9px
			}
		}
		@media (min-width: 576px) {
			.category {

				margin-top: -115px;
				margin-left: 9px
			}
		}


		@media (min-width: 768px) {
			.category {

				margin-top: -115px;
				margin-left: 204px
			}
		}


		@media (min-width: 992px) {

			.category {
				margin-top: 68px;
				margin-left: 57px
			}
		}


		@media (min-width: 1200px) {
		    :root {
    --size-divisor: .67;
  }
			.category {
				margin-top: -190px;
				margin-left: 563px
			}
		}

		.pn-ProductNav_Wrapper {
			position: relative;
			padding: 0 11px;
			box-sizing: border-box;
		}

		.pn-ProductNav {
			/* Make this scrollable when needed */
			overflow-x: auto;
			/* We don't want vertical scrolling */
			overflow-y: hidden;
			/* For WebKit implementations, provide inertia scrolling */
			-webkit-overflow-scrolling: touch !important;
			overscroll-behavior: auto contain;

			/* We don't want internal inline elements to wrap */
			white-space: nowrap;

		/* If JS present, let's hide the default scrollbar */
		.js

		&
		{
		/* Make an auto-hiding scroller for the 3 people using a IE */
			-ms-overflow-style: -ms-autohiding-scrollbar
		;
		/* Remove the default scrollbar for WebKit implementations */
		&
		::-webkit-scrollbar {
			display: none;
		}

		}
		/* positioning context for advancers */
		position: relative

		;
		/
		/
		Crush the whitespace here
		font-size:

		0
		;
		}

		.pn-ProductNav_Contents {
			float: left;
			transition: transform 0.2s ease-in-out;
			position: relative;
		}

		.pn-ProductNav_Contents-no-transition {
			transition: none;
		}

		.pn-ProductNav_Link {
			text-decoration: none;
			color: #888;
			font-size: 1.2rem;
			font-family: -apple-system, sans-serif;
			display: inline-flex;
			align-items: center;
			min-height: 44px;
			border: 1px solid transparent;
			padding: 0 11px;

		&number count
		+ & {
			border-left-color: #eee;
		}

		&
		[aria-selected="true"] {
			color: #111;
		}

		}

		.pn-Advancer {
			/* Reset the button */
			appearance: none;
			background: transparent;
			padding: 0;
			border: 0;

		&
		:focus {
			outline: 0;
		}

		&
		:hover {
			cursor: pointer;
		}

		/* Now style it as needed */
		position: absolute

		;
		top:

		0
		;
		bottom:

		0
		;
		/* Set the buttons invisible by default */
		opacity:

		0
		;
		transition: opacity

		0.3
		s

		;
		}

		.pn-Advancer_Left {
			left: 0;

		[data-overflowing="both"] ~ &,
		[data-overflowing="left"] ~ & {
			opacity: 1;
		}

		}

		.pn-Advancer_Right {
			right: 0;

		[data-overflowing="both"] ~ &,
		[data-overflowing="right"] ~ & {
			opacity: 1;
		}

		}

		.pn-Advancer_Icon {
			width: 20px;
			height: 44px;
			fill: #bbb;
		}

		.pn-ProductNav_Indicator {
			position: absolute;
			bottom: 0;
			left: 0;
			height: 4px;
			width: 100px;
			background-color: transparent;
			transform-origin: 0 0;
			transition: transform 0.2s ease-in-out,
			background-color 0.2s ease-in-out;
		}

		.card {
			background-color: #b44412;
			width: 100px;
			height: 100px;
			border-radius: 55px;
			text-align: center;
			padding-top: 20px;
			margin-right: 4px;
			border:solid 3px;
			border-color:#b5b5b5;
			/*border-radius: 10px;*/
		}
	</style>
</head>
<body style="background-image: url(<?php echo base_url('menu_assets/css/background-1.jpg')?>);
		background-size: auto;"


>
<span style="z-index:1;"  class="shopping"><a href="<?= base_url('Show/showShoppingCart') ?>"> <img width="40"  src="<?= base_url('uploads/cart.png') ?>"  /><button id="shop" style=" width: 26px;
    border-radius: 33px;
    margin-left: -4px;
    background-color: #ff1744; border-color:#000;color:#fff;font-size:16px"></button></a></span>

 <?php if (isset($_SESSION['role']) and $_SESSION['role']=='user'):?>
	<span style="z-index:1;"   class="signin"><a style="color: #fff;z-index:1" href="<?= base_url('Dashboard/User/showUserFactor') ?>"> پنل کاربری  </a></span>
<?php endif; ?>
	<?php if (isset($_SESSION['role']) and $_SESSION['role']=='customer'):?>
		<span style="z-index:1;"  class="signin"><a style="color: #fff;z-index:1" href="<?= base_url('Dashboard/User/showCustomerFactor') ?>">   پنل کاربری  </a></span>
	<?php endif; ?>
	<?php if (!isset($_SESSION['role'])):?>
		<span style="z-index:1;"  class="signin"><a href="<?= base_url('Auth/index') ?>"> <img src="<?= base_url('uploads/login.png') ?>"  style="width: 52px;color: #b44412; border-radius: 30p;z-index:1"></a></span>
	<?php endif; ?>


	<div class="col-sm-1" data-aos="fade-down" data-aos-duration="1000" style="width:100%;height100%">

		<span>

          <div style="background-color: #b44412" class="navlogo carpet">
            <img class="navmenu" style="" src="<?= base_url()?>menu_assets/logo.png" alt="موبایل کافه"/>
			<img href="" src=""/>

          </div>
        </span>
	</div>
</nav>
    <img class="navmenu imgLogo" src="<?= base_url()?>menu_assets/logo.png" alt="موبایل کافه"/>
<div>
    <div style=""  class=" nav_item pn-ProductNav_Wrapper custom-slider">
		<nav id="pnProductNav" class="pn-ProductNav">


			<div id="pnProductNavContents" class="pn-ProductNav_Contents" style="margin-top: 0px; display: flex">

				<?php foreach ($category2 as $cat2): ?>
				<!-- foods -->
				<div id="<?= sha1('yellowbeauty@139004' . md5($cat2['category_id']  . 'yellowbeauty@139004'))?>" class="type_menu det card <?= ' ' . sha1('yellowbeauty@139004' . md5($cat2['name']  . 'yellowbeauty@139004')) ?>">
					<img width="30" height="30" src="<?= $cat2['image_url'] ?>" alt=""/>
					</br>
					<span style="color: white;font-size: 14px;font-weight: bold;font-family: 'IRANSansWeb';"><?= $cat2['name'] ?></span>
				</div>
				<?php endforeach; ?>


				<span id="pnIndicator" class="pn-ProductNav_Indicator"></span>
			</div>
		</nav>
		<button
			id="pnAdvancerLeft"
			class="pn-Advancer pn-Advancer_Left"
			type="button"
		>
			<svg
				class="pn-Advancer_Icon"
				xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 551 1024"
			>
				<path
					d="M445.44 38.183L-2.53 512l447.97 473.817 85.857-81.173-409.6-433.23v81.172l409.6-433.23L445.44 38.18z"
				/>
			</svg>
		</button>
		<button
			id="pnAdvancerRight"
			class="pn-Advancer pn-Advancer_Right"
			type="button"
		>
			<svg
				class="pn-Advancer_Icon"
				xmlns="http://www.w3.org/2000/svg"
				viewBox="0 0 551 1024"
			>
				<path
					d="M105.56 985.817L553.53 512 105.56 38.183l-85.857 81.173 409.6 433.23v-81.172l-409.6 433.23 85.856 81.174z"
				/>
			</svg>
		</button>
	</div>



    
    <section id="menu_details">
        <div class="sec3-nav" data-aos="zoom-in-down" data-aos-duration="1000">
    
        <div id="foodlist3" class="foodlist row">
        <?php foreach ($items as $item): ?>
        
            <div style="display:none;" class="col-lg-4 <?=sha1('yellowbeauty@139004' . md5($item['category_id'] .'yellowbeauty@139004')) ?>">
                <div class="food" style="text-align:right"> <img src="<?= $item['img'] ?>" alt="">
                    <p class="discount"><?= $item['i_name'] ?></p>
                    <div class="food-text"> <a href="../foodinfo/index.html"><?= $item['description'] ?> </a>
                        <p style="color: #F8C200;"><?= number_format($item['price_special']) . ' ' . 'تومان'?> </p>
                    </div>
                    <div class="number count" style="margin:19px">
					

	
                    <button style="width: 28px;font-size: 20px;background-color:#f2d89b;color:#242425;height:28px;border-radius: 30px;border-color: #242425" class="minus" value="<?= $item['i_id'] ?>">-</button>
                    <input id="<?= 'in'.$item['i_id'] ?>" style="margin-top:5px;width: 35px;height:35px;font-size: 24px;text-align: center;margin-top:-6px;border-radius: 30px" class="counter" type="text" value="0"/>
                    <button style="width: 28px;font-size: 20px;background-color:#f2d89b;color:#242425;height:28px;border-radius: 30px;border-color: #242425" class="plus" value="<?= $item['i_id'] ?>">+</button>
        </div>
                </div>
            </div>
       
            <?php endforeach; ?>
        </div>
    </section>
        </div> 
</body>
<script>
	function openNav() {
		document.getElementById("myNav").style.height = "100%";
	}

	function closeNav() {
		document.getElementById("myNav").style.height = "0%";
	}

	function openTab(evt, tabId) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("foodlist row");
		console.log(tabcontent[1].style.display);
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("sec3-btn");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" selected", "");
		}
		document.getElementById(tabId).style.display = "flex";
		evt.currentTarget.className += " selected";
	}
</script>
<script>
	AOS.init();
</script>
<script>
	var SETTINGS = {
		navBarTravelling: false,
		navBarTravelDirection: "",
		navBarTravelDistance: 150,
	};

	var colours = {
		0: "#867100",
		1: "#7F4200",
		2: "#99813D",
		3: "#40FEFF",
		4: "#14CC99",
		5: "#00BAFF",
		6: "#0082B2",
		7: "#B25D7A",
		8: "#00FF17",
		9: "#006B49",
		10: "#00B27A",
		11: "#996B3D",
		12: "#CC7014",
		13: "#40FF8C",
		14: "#FF3400",
		15: "#ECBB5E",
		16: "#ECBB0C",
		17: "#B9D912",
		18: "#253A93",
		19: "#125FB9",
	};

	document.documentElement.classList.remove("no-js");
	document.documentElement.classList.add("js");

	// Out advancer buttons
	var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
	var pnAdvancerRight = document.getElementById("pnAdvancerRight");
	// the indicator
	var pnIndicator = document.getElementById("pnIndicator");

	var pnProductNav = document.getElementById("pnProductNav");
	var pnProductNavContents = document.getElementById("pnProductNavContents");

	pnProductNav.setAttribute(
		"data-overflowing",
		determineOverflow(pnProductNavContents, pnProductNav)
	);

	// Set the indicator
	moveIndicator(
		pnProductNav.querySelector('[aria-selected="true"]'),
		colours[0]
	);

	// Handle the scroll of the horizontal container
	var last_known_scroll_position = 0;
	var ticking = false;

	function doSomething(scroll_pos) {
		pnProductNav.setAttribute(
			"data-overflowing",
			determineOverflow(pnProductNavContents, pnProductNav)
		);
	}

	pnProductNav.addEventListener("scroll", function () {
		last_known_scroll_position = window.scrollY;
		if (!ticking) {
			window.requestAnimationFrame(function () {
				doSomething(last_known_scroll_position);
				ticking = false;
			});
		}
		ticking = true;
	});

	pnAdvancerLeft.addEventListener("click", function () {
		// If in the middle of a move return
		if (SETTINGS.navBarTravelling === true) {
			return;
		}
		// If we have content overflowing both sides or on the left
		if (
			determineOverflow(pnProductNavContents, pnProductNav) === "left" ||
			determineOverflow(pnProductNavContents, pnProductNav) === "both"
		) {
			// Find how far this panel has been scrolled
			var availableScrollLeft = pnProductNav.scrollLeft;
			// If the space available is less than two lots of our desired distance, just move the whole amount
			// otherwise, move by the amount in the settings
			if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
				pnProductNavContents.style.transform =
					"translateX(" + availableScrollLeft + "px)";
			} else {
				pnProductNavContents.style.transform =
					"translateX(" + SETTINGS.navBarTravelDistance + "px)";
			}
			// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
			pnProductNavContents.classList.remove(
				"pn-ProductNav_Contents-no-transition"
			);
			// Update our settings
			SETTINGS.navBarTravelDirection = "left";
			SETTINGS.navBarTravelling = true;
		}
		// Now update the attribute in the DOM
		pnProductNav.setAttribute(
			"data-overflowing",
			determineOverflow(pnProductNavContents, pnProductNav)
		);
	});

	pnAdvancerRight.addEventListener("click", function () {
		// If in the middle of a move return
		if (SETTINGS.navBarTravelling === true) {
			return;
		}
		// If we have content overflowing both sides or on the right
		if (
			determineOverflow(pnProductNavContents, pnProductNav) === "right" ||
			determineOverflow(pnProductNavContents, pnProductNav) === "both"
		) {
			// Get the right edge of the container and content
			var navBarRightEdge = pnProductNavContents.getBoundingClientRect()
				.right;
			var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect()
				.right;
			// Now we know how much space we have available to scroll
			var availableScrollRight = Math.floor(
				navBarRightEdge - navBarScrollerRightEdge
			);
			// If the space available is less than two lots of our desired distance, just move the whole amount
			// otherwise, move by the amount in the settings
			if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
				pnProductNavContents.style.transform =
					"translateX(-" + availableScrollRight + "px)";
			} else {
				pnProductNavContents.style.transform =
					"translateX(-" + SETTINGS.navBarTravelDistance + "px)";
			}
			// We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
			pnProductNavContents.classList.remove(
				"pn-ProductNav_Contents-no-transition"
			);
			// Update our settings
			SETTINGS.navBarTravelDirection = "right";
			SETTINGS.navBarTravelling = true;
		}
		// Now update the attribute in the DOM
		pnProductNav.setAttribute(
			"data-overflowing",
			determineOverflow(pnProductNavContents, pnProductNav)
		);
	});

	pnProductNavContents.addEventListener(
		"transitionend",
		function () {
			// get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
			var styleOfTransform = window.getComputedStyle(
				pnProductNavContents,
				null
			);
			var tr =
				styleOfTransform.getPropertyValue("-webkit-transform") ||
				styleOfTransform.getPropertyValue("transform");
			// If there is no transition we want to default to 0 and not null
			var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
			pnProductNavContents.style.transform = "none";
			pnProductNavContents.classList.add(
				"pn-ProductNav_Contents-no-transition"
			);
			// Now lets set the scroll position
			if (SETTINGS.navBarTravelDirection === "left") {
				pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
			} else {
				pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
			}
			SETTINGS.navBarTravelling = false;
		},
		false
	);

	// Handle setting the currently active link
	pnProductNavContents.addEventListener("click", function (e) {
		var links = [].slice.call(
			document.querySelectorAll(".pn-ProductNav_Link")
		);
		links.forEach(function (item) {
			item.setAttribute("aria-selected", "false");
		});
		e.target.setAttribute("aria-selected", "true");
		// Pass the clicked item and it's colour to the move indicator function
		moveIndicator(e.target, colours[links.indexOf(e.target)]);
	});

	// var count = 0;
	function moveIndicator(item, color) {
		var textPosition = item.getBoundingClientRect();
		var container = pnProductNavContents.getBoundingClientRect().left;
		var distance = textPosition.left - container;
		var scroll = pnProductNavContents.scrollLeft;
		pnIndicator.style.transform =
			"translateX(" +
			(distance + scroll) +
			"px) scaleX(" +
			textPosition.width * 0.01 +
			")";
		// count = count += 100;
		// pnIndicator.style.transform = "translateX(" + count + "px)";

		if (color) {
			pnIndicator.style.backgroundColor = color;
		}
	}

	function determineOverflow(content, container) {
		var containerMetrics = container.getBoundingClientRect();
		var containerMetricsRight = Math.floor(containerMetrics.right);
		var containerMetricsLeft = Math.floor(containerMetrics.left);
		var contentMetrics = content.getBoundingClientRect();
		var contentMetricsRight = Math.floor(contentMetrics.right);
		var contentMetricsLeft = Math.floor(contentMetrics.left);
		if (
			containerMetricsLeft > contentMetricsLeft &&
			containerMetricsRight < contentMetricsRight
		) {
			return "both";
		} else if (contentMetricsLeft < containerMetricsLeft) {
			return "left";
		} else if (contentMetricsRight > containerMetricsRight) {
			return "right";
		} else {
			return "none";
		}
	}

	/**
	 * @fileoverview dragscroll - scroll area by dragging
	 * @version 0.0.8
	 *
	 * @license MIT, see https://github.com/asvd/dragscroll
	 * @copyright 2015 asvd <heliosframework@gmail.com>
	 */

	(function (root, factory) {
		if (typeof define === "function" && define.amd) {
			define(["exports"], factory);
		} else if (typeof exports !== "undefined") {
			factory(exports);
		} else {
			factory((root.dragscroll = {}));
		}
	})(this, function (exports) {
		var _window = window;
		var _document = document;
		var mousemove = "mousemove";
		var mouseup = "mouseup";
		var mousedown = "mousedown";
		var EventListener = "EventListener";
		var addEventListener = "add" + EventListener;
		var removeEventListener = "remove" + EventListener;
		var newScrollX, newScrollY;

		var dragged = [];
		var reset = function (i, el) {
			for (i = 0; i < dragged.length;) {
				el = dragged[i++];
				el = el.container || el;
				el[removeEventListener](mousedown, el.md, 0);
				_window[removeEventListener](mouseup, el.mu, 0);
				_window[removeEventListener](mousemove, el.mm, 0);
			}

			// cloning into array since HTMLCollection is updated dynamically
			dragged = [].slice.call(_document.getElementsByClassName("dragscroll"));
			for (i = 0; i < dragged.length;) {
				(function (el, lastClientX, lastClientY, pushed, scroller, cont) {
					(cont = el.container || el)[addEventListener](
						mousedown,
						(cont.md = function (e) {
							if (
								!el.hasAttribute("nochilddrag") ||
								_document.elementFromPoint(e.pageX, e.pageY) == cont
							) {
								pushed = 1;
								lastClientX = e.clientX;
								lastClientY = e.clientY;

								e.preventDefault();
							}
						}),
						0
					);

					_window[addEventListener](
						mouseup,
						(cont.mu = function () {
							pushed = 0;
						}),
						0
					);

					_window[addEventListener](
						mousemove,
						(cont.mm = function (e) {
							if (pushed) {
								(scroller = el.scroller || el).scrollLeft -= newScrollX =
									-lastClientX + (lastClientX = e.clientX);
								scroller.scrollTop -= newScrollY =
									-lastClientY + (lastClientY = e.clientY);
								if (el == _document.body) {
									(scroller =
										_document.documentElement).scrollLeft -= newScrollX;
									scroller.scrollTop -= newScrollY;
								}
							}
						}),
						0
					);
				})(dragged[i++]);
			}
		};

		if (_document.readyState == "complete") {
			reset();
		} else {
			_window[addEventListener]("load", reset, 0);
		}

		exports.reset = reset;
	});
</script>
<script>
	$(document).ready(function () {
		$(".type_menu").on("click", function () {
			$(".det").css("background-color", "#023a3f");
			var kind = $(this).attr('id')
			// $("#menu_details").children().hide();
			$("." + kind).css("display", "block");
			$(this).css("background-color", "#b5b5b5");
			$(this).css("border-color", "#023a3f");

			$([document.documentElement, document.body]).animate({
				scrollTop: $("#menu_details").offset().top
			}, 2000);


		});
	});
</script>
<script>
$(document).ready(function() {

			$('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 1 ? 0 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
		});
</script>
<script>
	$(document).ready(function() {
		$('.plus').click(function () {
			var count = $(this).parent().find('input').val();
			var id=$(this).val();
			var obj={"id":id,"count":"1"};
			var res=localStorage.getItem("cart");


			if (res==null){
				res=[];
				res.push(obj);
				localStorage.setItem("cart", JSON.stringify(res));
			}else {
				var data=JSON.parse(localStorage.getItem("cart"))

				index = data.findIndex(x => x.id ===id);
				console.log(index);
				if (index>-1){
					data[0]['count']++;
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));
				}else{
					data.push(obj);
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));

				}

				// console.log(data);
				// data.push(obj);
				// localStorage.setItem("cart", JSON.stringify(data));

			}
            var res=JSON.parse(localStorage.getItem("cart"));
        var c=0;
        var newArr = res.map(function (val) {
										
										c+=parseInt(val['count']);
                                        var selector="#in"+val['id'];
                                        $(selector).val(val['count']);
									}
							);
                            $("#shop").html(c);


		});
		// $('.plus').click(function () {
		// 	var $input = $(this).parent().find('input');
		// 	$input.val(parseInt($input.val()) + 1);
		// 	$input.change();
		// 	return false;
		// });
	});
</script>
<script>
	$(document).ready(function() {
		$('.minus').click(function () {
			var count = $(this).parent().find('input').val();
			var id=$(this).val();
			// var obj={"id":id,"count":"1"};
			var data=JSON.parse(localStorage.getItem("cart"))

				index = data.findIndex(x => x.id ===id);
				console.log(index);
				if (data[0]['count']==1){
					data.splice(index, 1);
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));
				}else{
					data[0]['count']--;
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));

				}

				// console.log(data);
				// data.push(obj);
				// localStorage.setItem("cart", JSON.stringify(data));


			
            var res=JSON.parse(localStorage.getItem("cart"));
        var c=0;
        var newArr = res.map(function (val) {
										
										c+=parseInt(val['count']);
                                        var selector="#in"+val['id'];
                                        $(selector).val(val['count']);
									}
							);
                            $("#shop").html(c);


		});
	
	});
</script>
<script>
	$(document).ready(function() {
		
		var res=JSON.parse(localStorage.getItem("cart"));
        var c=0;
        var newArr = res.map(function (val) {
										
										c+=parseInt(val['count']);
                                        var selector="#in"+val['id'];
                                        $(selector).val(val['count']);
									}
							);
                            $("#shop").html(c);
                            
        
		});


</script>
<script>
    $(document).ready(function() {
   $('input').change(function()  {
      var count = 0;
      $("input").each(function() {           
         if($(this).val() == "0")
         {
            $(this).removeAttr("disabled","disabled");
         }
       });          

      });
   });
</script>
</html>