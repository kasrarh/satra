
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Checkout example for Bootstrap</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/checkout/">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('portals/') ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
    
  </head>

  <body style="background-image: url(<?php echo base_url('menu_assets/css/background-1.jpg')?>);
		background-size: auto; font-family:iranSans !important;"


>

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="<?= base_url()?>menu_assets/logo.png" alt="" width="150" height="150">
       </div>
      

      <div class="row p-3" style="background-color: #ffff; border-radius:50px " >
      <div class="col-12 col-md-12 col-sm-12" style="direction:rtl; text-align:center;overflow-y: auto; height: 100%;">
      <table class="table table-bordered events-table " style="overflow: scroll; display: block ;  max-width: -moz-fit-content;
  max-width: fit-content;
  margin: 0 auto;
  overflow-x: auto;
  white-space: nowrap;">
  <thead>
    <tr>
    
      <th scope="col" colspan="1">نام محصول</th>
      <th scope="col" colspan="4">تعداد</th>
      <th scope="col" colspan="1">مبلغ</th>
      <th scope="col" colspan="1">جمع کل</th>
    </tr>
  </thead>
  <tbody class="newField_wrapper">
  
  </tbody>
  <tfoot>
    <tr>
      <td>مبلغ کل</td>
      <td colspan="6" id="totalPrice"></td>
    </tr>
  </tfoot>
  
</table>

<a href="<?= base_url('Show/index') ?>"><button class="btn btn-primary btn-lg btn-block">بازگشت به صفحه اصلی </button></a>
<?php if($_SESSION['isUser']){ ?>
        <div class="col-md-12" style="direction: rtl; text-align:right; padding:10px">
          <h4 class="mb-3">مشخصات سفارش</h4>
          <form class="needs-validation" method="post">
            
          
            
            <div class="mb-3">
             <label for="address"> <h5> نشانی جدید:</h5></label>
              <br>
              <textarea class="form-control" name="address" id="addressUser" cols="50" rows="5"></textarea>
              <div class="invalid-feedback" style="font-size:12px !important;">
                لطفا آدرس خود را وارد نمایید
              </div>
            </div>
            <div class="custom-control custom-checkbox">
            
              <input type="checkbox" name="checkbox" class="custom-control-input form-control" id="sameAddress">
             <label class="custom-control-label"  style="font-size:12px !important;" for="sameAddress"> <h6>استفاده از نشانی ثبت شده در حساب کاربری</h6></label>
              
            
            </div>
            </div>
            
            
            <hr class="mb-4">
            <button class="btn btn-success btn-lg btn-block" type="submit" style="color:white" id="submitUser" name="submitUser">ثبت خرید</button>
          </form>
        </div>
        <?php }else{ ?>
          <div class="col-md-12" style="direction: rtl; text-align:right; padding:10px">
          <h4 class="mb-3">مشخصات سفارش</h4>
          <h6 id="alert" style="color: red;display: none">تمامی فیلد های زیر را پر کنید.</h6>
          <form method="post" class="needs-validation form-group" >
            <div class="form-group">
              <h2><label for="name" style="font-size:12px !important;">نام و نام خانوادگی</label></h2>
              <input type="text" class="form-control" name="name" id="name" placeholder="نام خود را وارد کنید" required>
            </div>
            <div class="form-group">
              <h2><label for="phone" style="font-size:12px !important;">شماره تماس</label></h2>
              <input type="text" name="phone" class="form-control" id="phone" placeholder="شماره خود را وارد کنید"  required>
            </div>

            <div class="mb-3">
              <h2><label for="address" style="font-size:12px !important;"> نشانی </label></h2>
              <textarea name="address" id="addressCustomer" cols="30" rows="4" required></textarea>

            </div>
            <hr class="mb-4">
            <button class="btn btn-success btn-lg btn-block" style="color:white" id="submitCustomer" type="submit" name="submitCustomer">ثبت خرید</button>
          </form>
        </div>
        <?php } ?>

      </div>

      <footer class="my-5 pt-5 text-muted text-center text-small">
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url('portals/assets/') ?>js/jquery-3.3.1.min.js"></script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="<?php echo base_url('portals/assets/') ?>/bootstrap.min.js"></script>
    <script src="../../assets/js/vendor/holder.min.js"></script>
      <!-- <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
          'use strict';

          window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
      </script> -->
    
    <script>
    $(function() {
  enable_cb();
  $("#sameAddress").click(enable_cb);
});

function enable_cb() {
  if (this.checked) {
    $("#addressUser").attr("disabled", true);
  } else {
    $("#addressUser").removeAttr("disabled");
  }
}
</script>
<?php if($_SESSION['isUser']){ ?>}
<script type="text/javascript">
$(document).ready(function () {
  var data=JSON.parse(localStorage.getItem("cart"));
  
  
  $.ajax({
						url: '<?php echo base_url('Show/getItemsLocalstorage')?>',
						method: 'POST',
            data: {data:JSON.stringify(data)},
						dataType: 'json',
						success: function (response) {
            
             
              var totalPrice=0;
              response.map(function (val) {
                function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
              
              var fieldHTML = '<tr>\n'+
    '<td colspan="1">'+ val[0]['name']+ '</td>\n'+
    '<td style="width:100px;" colspan="4">'+ val[1]['count'] +'\n'+
    '</td>\n'+
           '<td colspan="1">'+ numberWithCommas(val[0]['price_special']) + 'تومان</td>\n'+
          '<td colspan="1">'+ numberWithCommas(parseInt(val[0]['price_special'])*parseInt(val[1]['count'])) +' تومان</td>\n'+
  '</tr>';
            
          
  $(".newField_wrapper").append(fieldHTML); //Add field html
          
          var count=parseInt(val[1]['count']);  
          var price=parseInt(val[0]['price_special']);  
          totalPrice=totalPrice+(price*count);
          
          $("#totalPrice").html( numberWithCommas(totalPrice) +' تومان');
          
          });
          
         

              
						}
					}
			);
     
		});

</script>
<?php }else{ ?>
<script type="text/javascript">
$(document).ready(function () {
  var data=JSON.parse(localStorage.getItem("cart"));
  
  
  $.ajax({
						url: '<?php echo base_url('Show/getItemsLocalstorage')?>',
						method: 'POST',
            data: {data:JSON.stringify(data)},
						dataType: 'json',
						success: function (response) {
              var totalPrice=0;
              
              response.map(function (val) {
                function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
              
              var fieldHTML = '<tr>\n'+
    '<td colspan="1">'+ val[0]['name']+ '</td>\n'+
    '<td style="width:100px;" colspan="4">'+ val[1]['count'] +'\n'+
    
    '</td>\n'+
           '<td colspan="1">'+ numberWithCommas(val[0]['price_normal']) +'تومان</td>\n'+
          '<td colspan="1">'+ numberWithCommas(parseInt(val[0]['price_normal'])*parseInt(val[1]['count'])) +' تومان</td>\n'+
  '</tr>';
            
          
  $(".newField_wrapper").append(fieldHTML); //Add field html
          var count=parseInt(val[1]['count']);  
          var price=parseInt(val[0]['price_normal']);  
          totalPrice=totalPrice+(price*count);
        
          $("#totalPrice").html( numberWithCommas(totalPrice) +' تومان');
          });
          
      
              
						}
					}
			);
		});


</script>
<?php } ?>
<script>
$(document).ready(function(){
  var data=JSON.parse(localStorage.getItem("cart"));
  $("#submitCustomer").click(function(e){
    e.preventDefault();
    var info={name:$("#name").val(),phone:$("#phone").val(),address:$("#addressCustomer").val()};
    console.log(JSON.stringify(info));
    console.log(JSON.stringify(data));
    if($("#name").val() && $("#phone").val() && $("#addressCustomer").val() != null){
     $.ajax({
						url: '<?php echo base_url('Show/submitCustomerFactor')?>',
						method: 'POST',
            data: {data:JSON.stringify(data),info:JSON.stringify(info)},
						dataType: 'json',
            success: function (response) {

				window.location.href = "<?= base_url() ?>/Shop/Shop";
        

            }
     });
    }else{
      $("#alert").css("display","block");
          }
  });
  $("#submitUser").click(function(e){
    e.preventDefault();
    var info={address:$("#addressUser").val(),sameAddress:$("#sameAddress").val()};
    console.log(JSON.stringify(info));
    console.log(JSON.stringify(data));
     $.ajax({
						url: '<?php echo base_url('Show/submitUserFactor')?>',
						method: 'POST',
            data: {data:JSON.stringify(data),info:JSON.stringify(info)},
						dataType: 'json',
            success: function (response) {

				window.location.href = "<?= base_url() ?>/Shop/Shop";

            }
     });
  });
});
</script>
<script>
$(document).ready(function() {
			$('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 1 ? 0 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
		});
</script>
<script>
	$(document).ready(function() {
		$('.plus').click(function () {
      console.log(1);
			var count = $(this).parent().find('input').val();
			var id=$(this).val();
			var obj={"id":id,"count":"1"};
			var res=localStorage.getItem("cart");


			if (res==null){
				res=[];
				res.push(obj);
				localStorage.setItem("cart", JSON.stringify(res));
			}else {
				var data=JSON.parse(localStorage.getItem("cart"))
				

				index = data.findIndex(x => x.id ===id);
				console.log(index);
				if (index>-1){
					data[index]['count']++;
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));
				}else{
					data.push(obj);
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));

				}

				// console.log(data);
				// data.push(obj);
				// localStorage.setItem("cart", JSON.stringify(data));

			}
                           


		});
		// $('.plus').click(function () {
		// 	var $input = $(this).parent().find('input');
		// 	$input.val(parseInt($input.val()) + 1);
		// 	$input.change();
		// 	return false;
		// });
	});
</script>
<script>
	$(document).ready(function() {
		$('.minus').click(function () {
			var count = $(this).parent().find('input').val();
			var id=$(this).val();
			// var obj={"id":id,"count":"1"};
			var data=JSON.parse(localStorage.getItem("cart"))

				index = data.findIndex(x => x.id ===id);
				console.log(index);
				if (data[index]['count']==1){
					data.splice(index, 1);
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));
				}else{
					data[index]['count']--;
					localStorage.removeItem("cart");
					localStorage.setItem("cart", JSON.stringify(data));

				}

				// console.log(data);
				// data.push(obj);
				// localStorage.setItem("cart", JSON.stringify(data));


			
       


		});
	
	});
</script>
<script>
    $(document).ready(function() {
   $('input').change(function()  {
      var count = 0;
      $("input").each(function() {           
         if($(this).val() == "0")
         {
            $(this).removeAttr("disabled","disabled");
         }
       });          

      });
   });
</script>



<!-- <script>
	$(document).ready(function() {
		
		var res=JSON.parse(localStorage.getItem("cart"));
        var c=0;
        var newArr = res.map(function (val) {
										
										c+=parseInt(val['count']);
                                        var selector="#in"+val['id'];
                                        $(selector).val(val['count']);
									}
							);
                            $("#shop").html(c);
                            
        
		});


</script> -->
  </body>
</html>
