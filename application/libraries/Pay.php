<?php

//remember to change the Merchant ID before any Implementation


class Pay extends CI_Model
{

	public function __construct()
	{
		parent::__construct();

	}

	function PayRequest($TotalPrice,$CallbackURL)
	{
		$_SESSION['TotalPrice']=$TotalPrice;
		$data = array(
			'MerchantID' => 'a6e4946d-0caa-4a96-bf7b-f6ac45d39741',
			'Amount' => $TotalPrice,
			'CallbackURL' => base_url().$CallbackURL,
			'Description' => 'کافه قنادی آنی');
		$jsonData = json_encode($data);
	
		$ch = curl_init('https://www.zarinpal.com/pg/rest/WebGate/PaymentRequest.json');
		curl_setopt($ch, CURLOPT_USERAGENT, 'ZarinPal Rest Api v1');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($jsonData)
		));
		$result = curl_exec($ch);
		$err = curl_error($ch);
		$result = json_decode($result, true);
		curl_close($ch);
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			if ($result["Status"] == 100) {
				header('Location: https://www.zarinpal.com/pg/StartPay/' . $result["Authority"]);
			} else {
				echo 'ERR: ' . $result["Status"];
			}
		}

	}
}
