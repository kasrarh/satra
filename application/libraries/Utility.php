<?php


class Utility extends CI_Model
{

	public function __construct()
	{
//		$this->load->library('encrypt');
		$this->load->helper("file");

	}

	public function add($view, $info, $table, $redirectPage, $data = null, $data2 = array(), $data3 = array(), $data4 = array(), $path = 'uploads/', $path2 = 'uploads/', $name = 'files', $name2 = 'files')
	{


		$this->load->view($view, array('data' => $data, 'data2' => $data2, 'data3' => $data3, 'data4' => $data4));
		if (isset($_POST['submit'])) {
			if (!empty($_FILES['files']['name'])) {
				$file_path = $this->uploadFile2($path, $name);
			}
			if (!empty($_FILES['files2']['name'])) {
				$file_path2 = $this->uploadFile2($path2, $name2);
			}
			$info2 = $info;

			if ($file_path != null) {
				$info2['image_url'] = $file_path;
			}
			if ($file_path2 != null) {
				$info2['file_url'] = $file_path2;
			}
			$res = $this->db->insert($table, $info2);
			if ($res) {
				$_SESSION['success'] = 'با موفقیت ثبت شد';
			} else {
				$_SESSION['error'] = 'خطایی در هنگام ثبت رخ داده است';

			}
			redirect($redirectPage);
		}

	}

	public function getAll($table)
	{
		$data = $this->db->get($table)->result_array();
		return $data;

	}

	public function getAllJoin($view, $table, $joiner, $field, $field2, $option = null, $option2 = null, $option3 = null, $option4 = null,$option5 = null,$option6 = null)
	{
		$this->db->select('*,' . $option . ',' . $option2 . ',' . $option3 . ',' . $option4 . ',' . $option5 . ',' . $option6);
		$this->db->from($table);
		$this->db->join($joiner, $table . '.' . $field . '=' . $joiner . '.' . $field2);
		$data = $this->db->get()->result_array();
		$this->load->view($view, array('data' => $data));

	}
	public function getAllJoin2($view, $table, $joiner, $field, $field2, $option = null, $option2 = null, $option3 = null, $option4 = null,$option5 = null,$option6 = null)
	{
		$this->db->select('*,' . $option . ',' . $option2 . ',' . $option3 . ',' . $option4 . ',' . $option5 . ',' . $option6);
		$this->db->from($table);
		$this->db->where('status',1);
		$this->db->join($joiner, $table . '.' . $field . '=' . $joiner . '.' . $field2);
		$data = $this->db->get()->result_array();
		$this->load->view($view, array('data' => $data));

	}

	

	public function getWithId($table,$id,$field='id')
	{
		$this->db->where($field,$id);
		$data = $this->db->get($table)->result_array();
		return $data;
	}
	public function getById($view, $table, $field, $id, $table2 = null, $field2 = null, $id2 = null)
	{

		$this->db->where($field, $id);
		$data = $this->db->get($table)->result_array();
		if ($table2 != null) {
			$this->db->where($field2, $id2);
			$data2 = $this->db->get($table2)->result_array();
		}
		$this->load->view($view, array('data' => $data, 'data2' => $data2));
	}

	public function edit($view, $table, $filed, $id, $info, $redirectPage, $table2 = null, $path = 'uploads/', $path2 = 'uploads/', $name = 'files', $name2 = 'files', $data3 = null)
	{
		$this->db->where($filed, $id);
		$data = $this->db->get($table)->result_array();
		if ($table2 != null) {
			$data2 = $this->db->get($table2)->result_array();
			$this->load->view($view, array('data' => $data, 'data2' => $data2, 'data3' => $data3));
		}else{
			$this->load->view($view, array('data' => $data,'data3' => $data3));
		}

		if (isset($_POST['submit'])) {
			if (!empty($_FILES['files']['name'])) {
				$file_path = $this->uploadFile2($path, $name);
			}
			if (!empty($_FILES['files2']['name'])) {
				$file_path2 = $this->uploadFile2($path2, $name2);
			}
			$info2 = $info;
			if ($file_path != null) {
				$info2['image_url'] = $file_path;
				$image_url = $this->db->get_where($table, array($filed => $id))->row()->image_url;
				unlink($image_url);
			}

			if ($file_path2 != null) {
				$info2['file_url'] = $file_path2;
				$file_url = $this->db->get_where($table, array($filed => $id))->row()->image_url;
				unlink($file_url);
			}
			$this->db->where($filed, $id);
			$res = $this->db->update($table, $info2);
			if ($res) {
				$_SESSION['success'] = 'با موفقیت ویرایش شد';
			} else {
				$_SESSION['error'] = 'خطایی در هنگام ویرایش رخ داده است';

			}
			redirect($redirectPage);
		}

	}

	public function delete($redirectPage, $table, $field, $id)
	{
		$this->db->from($table);
		$this->db->where($field, $id);
		$res = $this->db->delete();
		if ($res) {
			$_SESSION['success'] = 'با موفقیت حذف شد';
		} else {
			$_SESSION['error'] = 'خطایی در هنگام حذف رخ داده است';

		}
		redirect($redirectPage);

	}

	public function hash($msg)
	{

		$key = 'amirkabir!@#$%^&*()_+';
		$encrypted_string = $this->encrypt->encode($msg, $key);
		return $encrypted_string;
	}

	public function unhash($msg)
	{

		$key = 'amirkabir!@#$%^&*()_+';
		$encrypted_string = $this->encrypt->decode($msg, $key);
		return $encrypted_string;
	}

	public function uploadFile()
	{

		if (isset($_POST['submit']) && !empty($_FILES['files']['name'])) {
			$fileCount = count($_FILES['files']['name']);
			for ($i = 0; $i < $fileCount; $i++) {
				$_FILES['file']['name'] = $_FILES['files']['name'][$i];
				$_FILES['file']['type'] = $_FILES['files']['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES['files']['error'][$i];
				$_FILES['file']['size'] = $_FILES['files']['size'][$i];

				//file upload configuration
				$config['upload_path'] = 'uploads/';
				$config['allowed_types'] = 'gif|jpg|png';

				//load  upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				//upload file to server
				if ($this->upload->do_upload('file')) {
					//uploaded file data
					$fileData = $this->upload->data();
					$uploadData[$i]['image_url'] = base_url() . './uploads/' . $fileData['file_name'];
				}
			}

			if (!empty($uploadData)) {
				$dataa = array();
				for ($i = 0; $i < 4; $i++) {
					@$dataa[$i] = $uploadData[$i]["image_url"];
				}


			}
			return $dataa[0];
		}
	}

	public function uploadFile2($path = 'uploads/', $name = 'files')
	{

		if (isset($_POST['submit']) && !empty($_FILES[$name]['name'])) {
			$fileCount = count($_FILES['files']['name']);
			for ($i = 0; $i < $fileCount; $i++) {
				$_FILES['file']['name'] = $_FILES[$name]['name'][$i];
				$_FILES['file']['type'] = $_FILES[$name]['type'][$i];
				$_FILES['file']['tmp_name'] = $_FILES[$name]['tmp_name'][$i];
				$_FILES['file']['error'] = $_FILES[$name]['error'][$i];
				$_FILES['file']['size'] = $_FILES[$name]['size'][$i];

				//file upload configuration
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'jpg|png|jpeg|svg|SVG';

				//load  upload library
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				//upload file to server
				if ($this->upload->do_upload('file')) {
					//uploaded file data
					$fileData = $this->upload->data();
					$uploadData[$i]['image_url'] = base_url() . $path . $fileData['file_name'];
				}
			}

			if (!empty($uploadData)) {
				$dataa = array();
				for ($i = 0; $i < 4; $i++) {
					@$dataa[$i] = $uploadData[$i]["image_url"];
				}

				return $dataa[0];
			}

		}
	}


}
